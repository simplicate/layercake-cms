<? class ContentRoute extends CakeRoute {

    // todo: comment this code
    function parse( $url ) {
        $params = parent::parse($url);
        if (empty($params)) {
            return false;
        }

        if( isset( $params['_args_'] ) ) {
            $params['_args_'] = preg_replace( '/\/$/', '', $params['_args_'] );
        }


        //$slugs = Cache::read( 'page_slugs' );

        if( empty($slugs) ) {

            App::import( 'Model', 'Content.Page' );
            $Page = new Page();

            $pages = $Page->find( 'all', array(
                'fields'    => 'slug',
                'recursive' => -1,
                'conditions' => array( 'status'    => 'active' ),
            ));

            $slugs = array_flip( Set::extract( $pages, '{n}.Page.slug') );

            if( !in_array( "/", array_keys( $slugs ) ) ) {
                $slugs["/eng/"] = 1;
                $slugs["/fre/"] = 1;

                App::import( 'Model', 'Content.Redirect' );
                $Redirect = new Redirect();

                $redirecthome = $Redirect->find( 'first', array(
                    'fields'    => 'slug',
                    'recursive' => -1,
                    'conditions' => array( 'status' => 'active', 'slug' => '/' ),
                ));

                if( !isset( $redirecthome['Redirect']['slug'] ) ) {
                    $slugs["/"]     = 1;
                }
            }

            // store to cache
            Cache::write( 'page_slugs', $slugs );
        }

        if(
            ( isset( $params['_args_']  ) && isset( $slugs[ '/' . $params['_args_']] )       ) ||
            ( isset( $params['_args_']  ) && isset( $slugs[ '/' . $params['_args_'] . '/'] ) ) ||
            ( !isset( $params['_args_'] ) && isset( $slugs[ '/' ] ) )
            ) {
            return $params;
        }

        if( isset( $_GET['mode'] ) && $_GET['mode'] == 'preview' ) {
            return $params;
        }

        return false;
    }

} ?>