<? class RedirectRoute extends CakeRoute {

    // todo: comment this code
    function parse( $url ) {
        $params = parent::parse($url);

        if( empty( $params ) ) {
            return false;
        }

        if( isset( $params['_args_'] ) ) {
            $params['_args_'] = preg_replace( '/\/$/', '', $params['_args_'] );
        }

        //$slugs = Cache::read( 'redirect_slugs' );

        if( empty($slugs) ) {

            App::import( 'Model', 'Content.Redirect' );
            $Redirect = new Redirect();

            $redirects = $Redirect->find( 'all', array(
                'fields'    => 'slug',
                'recursive' => -1,
                'conditions' => array( 'status' => 'active' ),
            ));

            $slugs = array_flip( Set::extract( $redirects, '{n}.Redirect.slug') );
            $slugs = array_change_key_case( $slugs, CASE_LOWER );

            // store to cache
            Cache::write( 'redirect_slugs', $slugs );
        }

        if( ( isset( $params['_args_'] ) && ( isset( $slugs[ '/' . strtolower( $params['_args_'] ) ]) || isset( $slugs[ '/' . strtolower( $params['_args_'] ) . '/']) ) ) || !isset( $params['_args_'] ) ) {
            return $params;
        }

        return false;
    }

} ?>