<? class MenuItem extends AppModel {

	var $name   = 'MenuItem';
    var $actsAs = array( 'Tree' );

	var $validate = array(
		'menu_id' => array('numeric'),
		'title' => array('notempty'),
		'link' => array('notempty'),
		'parent_id' => array('numeric'),
	);

	var $belongsTo = array(
		'Menu' => array(
			'className' => 'Content.Menu',
			'foreignKey' => 'menu_id',
		)
	);

    function addScope( $menu_id = null ) {
        if( isset( $menu_id ) ) {
            $this->Behaviors->detach( 'Tree' );
            $this->Behaviors->attach( 'Tree', array( 'scope' => "MenuItem.menu_id = {$menu_id}" ) );
        }
    }
}