<?php
class SupportTagUrl extends AppModel {
	var $name = 'SupportTagUrl';

	var $belongsTo = array(
		'SupportTag' => array(
			'className' => 'Content.SupportTag',
			'foreignKey' => 'support_tag_id',
		)
	);

    // touch up the way the urls look before displaying them to the user
    function afterFind( $results, $primary ) {

        // model name
        $modelName = isset( $this->alias ) ? $this->alias : $this->name;

        // loop through each result
        foreach( $results AS &$item ) {

            // don't bother if we don't have a url field
            if( !isset( $item[$modelName]['url'] ) ) { continue; }

            // if there is a wildcard
            if( strstr( $item[$modelName]['url'], '*' ) ) {

                // replace all stars with slashs
                $item[$modelName]['url'] = str_replace( '*', '/', $item[$modelName]['url'] );

                // add a trailing /* to the end
                $item[$modelName]['url'] .= '/*';

                // fix any double slashs
                $item[$modelName]['url'] = str_replace( '//', '/', $item[$modelName]['url'] );

            // otherwise it's just a regular url
            } else {

                // make sure the url starts and ends with a slash
                $item[$modelName]['url'] = '/' . $item[$modelName]['url'] . '/';

                // yes i mean to do this twice, to capture the odd ///
                $item[$modelName]['url'] = str_replace( '//', '/', $item[$modelName]['url'] );
                $item[$modelName]['url'] = str_replace( '//', '/', $item[$modelName]['url'] );
            }
        }

        return $results;
    }


    // format the urls better for saving in the database
    function beforeSave( $options ) {

        // model name
        $modelName = isset( $this->alias ) ? $this->alias : $this->name;

        if( isset( $this->data[$modelName]['url'] ) ) {

            // if there is a wild card
            if( strstr( $this->data[$modelName]['url'], '*' ) ) {

                // add a star to the start and end of the string
                $this->data[$modelName]['url'] = '*' . $this->data[$modelName]['url'] . '*';

                // replace all slashes with stars
                $this->data[$modelName]['url'] = str_replace( '/',  '*', $this->data[$modelName]['url'] );

                // replace any double (and triple) stars
                $this->data[$modelName]['url'] = str_replace( '**', '*', $this->data[$modelName]['url'] );
                $this->data[$modelName]['url'] = str_replace( '**', '*', $this->data[$modelName]['url'] );

            // otherwise it's just a regular url
            } else {

                // make sure the url starts and ends with a slash
                $this->data[$modelName]['url'] = '/' . $this->data[$modelName]['url'] . '/';

                // yes i mean to do this twice, to capture the odd ///
                $this->data[$modelName]['url'] = str_replace( '//', '/', $this->data[$modelName]['url'] );
                $this->data[$modelName]['url'] = str_replace( '//', '/', $this->data[$modelName]['url'] );
            }
        }

        // return true
        return 1;
    }
}
?>