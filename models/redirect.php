<?php
class Redirect extends AppModel {

	var $name = 'Redirect';
	var $validate = array(
		'slug'         => array('notempty'),
        'redirect_url' => array('notempty')
	);


    function beforeSave() {

        // touch-up slug
        if( isset( $this->data['Redirect']['slug'] ) && !empty( $this->data['Redirect']['slug'] ) ) {
            if( $this->data['Redirect']['slug'] != '/' ) {
                $this->data['Redirect']['slug'] = preg_replace( "/\/$/", "", $this->data['Redirect']['slug'] );
            }

            if( strstr( $this->data['Redirect']['slug'], 'http://' ) || strstr( $this->data['Redirect']['slug'], 'https://' ) ) {
                $url_parts = parse_url( $this->data['Redirect']['slug'] );
                $this->data['Redirect']['slug'] = $url_parts['path'];
            } else {
                $this->data['Redirect']['slug'] = "/" . $this->data['Redirect']['slug'];
                $this->data['Redirect']['slug'] = str_replace( "//", "/", $this->data['Redirect']['slug'] );
            }
        }

        // touch-up redirect url
        if( isset( $this->data['Redirect']['redirect_url'] ) && !empty( $this->data['Redirect']['redirect_url'] ) ) {
            if( strstr( $this->data['Redirect']['redirect_url'], 'http://' ) || strstr( $this->data['Redirect']['redirect_url'], 'https://' ) ) {
                // do nothing
            } else {
                $this->data['Redirect']['redirect_url'] = "/" . $this->data['Redirect']['redirect_url'];
                $this->data['Redirect']['redirect_url'] = str_replace( "//", "/", $this->data['Redirect']['redirect_url'] );
            }
        }

        if( !isset( $this->data['Redirect']['id'] ) ) {
            $this->data['Redirect']['status'] = 'active';
        }

        return 1;
    }

} ?>