<? class Menu extends AppModel {

	var $name = 'Menu';
	var $validate = array(
		'title' => array('notempty'),
		'slug' => array('notempty')
	);

	var $hasMany = array(
		'MenuItem' => array(
			'className' => 'Content.MenuItem',
			'foreignKey' => 'menu_id',
			'dependent' => false,
		)
	);
} ?>