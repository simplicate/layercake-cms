<?php
class SupportTag extends AppModel {
	var $name = 'SupportTag';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'You must give this tag a reference name',
			),
		),
		'location' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'You must select a location for this support tag',
			),
		)
	);

	var $hasMany = array(
		'SupportTagUrl' => array(
			'className' => 'Content.SupportTagUrl',
			'foreignKey' => 'support_tag_id',
			'dependent' => false,
			'order' => "LENGTH(url) DESC",
		)
	);
}
?>