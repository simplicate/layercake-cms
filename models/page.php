<?php
class Page extends AppModel {

	var $name = 'Page';
	var $validate = array(
		'name' => array('notempty')
	);

    var $actsAs = array(
        'Search.Searchable' => array( 'ignoreColumns' => array( 'status', 'user_status', 'language' ), 'summaryColumns' => array( 'content' ) ),
        'Containable'
	);

	var $belongsTo = array(
		'Layout' => array(
			'className' => 'Content.Layout',
			'foreignKey' => 'layout_id',
		)
	);

    var $hasMany = array(
        'PageHistory' => array(
            'className' => 'Content.PageHistory',
            'foreignKey' => 'page_id',
        )
    );

    function beforeSave() {

        if( isset( $this->data['Page']['slug'] ) && !empty( $this->data['Page']['slug'] ) ) {

            if( $this->data['Page']['slug'] != '/' ) {
                $this->data['Page']['slug'] = preg_replace( "/\/$/", "", $this->data['Page']['slug'] );
            }

            if( substr( $this->data['Page']['slug'], 0, 5 ) == '/eng/' || $this->data['Page']['slug'] == '/eng' ) {
                $this->data['Page']['language'] = 'eng';
            }

            if( substr( $this->data['Page']['slug'], 0, 5 ) == '/fre/' || $this->data['Page']['slug'] == '/fre' ) {
                $this->data['Page']['language'] = 'fre';
            }
        }

        if( !isset( $this->data['Page']['id'] ) ) {
            $this->data['Page']['status'] = 'active';
        }

        return 1;
    }


    function afterSave() {

        $history = $this->data['Page'];
        $history['page_id'] = $this->id;
        unset( $history['id'] );
        $this->PageHistory->save( array( "PageHistory" => $history ) );

        return 1;
    }

} ?>