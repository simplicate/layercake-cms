<? class Layout extends AppModel {

	var $name   = 'Layout';

    function snippet_locations( $layout_id = null ) {

        if( $layout_id ) {

            $layout = $this->findById( $id );
            $locations = explode( ",", $layout['Layout']['snippet_locations'] );
            return $locations;

        } else {

            $locations = array();
            $layouts = $this->find( 'all' );
            foreach ( $layouts AS $layout ) {
                $locations = array_merge( $locations, explode( ",", $layout['Layout']['snippet_locations'] ) );
            }

            $locations = array_unique( $locations );
            return $locations;
        }

    }

} ?>