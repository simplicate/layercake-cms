<?php
class PageHistory extends AppModel {

	var $name = 'PageHistory';

	var $belongsTo = array(
		'Page' => array(
			'className' => 'Content.Page',
			'foreignKey' => 'page_id',
		),
	);

    function revert() {

        $history = $this->data['Page'];
        $history['page_id'] = $history['id'];
        unset( $history['id'] );
        $this->PageHistory->save( array( "PageHistory" => $history ) );

        return 1;
    }
} ?>