<?php
class Snippet extends AppModel {
	var $name = 'Snippet';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		//'location' => array(
		//	'notempty' => array(
		//		'rule' => array('notempty'),
		//		//'message' => 'Your custom message here',
		//		//'allowEmpty' => false,
		//		//'required' => false,
		//		//'last' => false, // Stop validation after this rule
		//		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		//	),
		//),
		'ordering' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'SnippetTemplate' => array(
			'className' => 'Content.SnippetTemplate',
			'dependent' => false,
		)
	);

	var $hasMany = array(
		'SnippetUrl' => array(
			'className' => 'Content.SnippetUrl',
			'foreignKey' => 'snippet_id',
			'dependent' => false,
			'order' => "LENGTH(url) DESC",
		)
	);

}
?>