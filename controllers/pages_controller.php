<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class PagesController extends LayerCakeAppController {

	var $name       = 'Pages';
    var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'LayerCake.MenuTree', 'LayerCake.ThisUrl' );
    var $components = array( 'Session', 'Auth', 'LayerCake.PersistentValidation', 'Content.PageContent' );
    var $uses       = array(
						'Content.Page',
						'LayerCake.SearchIndex',
					  );

	function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'view' );
	}

    function error404() {
        $this->header("HTTP/1.0 404 Not Found");
        $this->render();
    }

    function error401() {
        $this->header("HTTP/1.1 401 Unauthorized");
        $this->render();
    }

	function view() {

        if( Configure::read('debug') == 0 ) {
            $this->helpers[]   = 'Cache';
            $this->cacheAction = '10 minutes';
        }

        // load page
        $page = $this->PageContent->load_for_this_url();

        // is the visitor logged in?
        $user_status = isset( $_SESSION['User']['User']['id'] ) ? 'logged-in' : 'logged-out';
        $user_group  = isset( $_SESSION['User']['UserGroup']['id'] ) ? $_SESSION['User']['UserGroup']['id'] : 0;

        if( isset( $_SESSION['Auth']['Admin']['id'] ) && isset( $this->params['url']['mode'] ) && isset( $this->params['url']['page_history_id'] ) && $this->params['url']['mode'] == 'preview' ) {
            $history = $this->Page->PageHistory->find( 'first', array( 'conditions' => array( 'PageHistory.id' => $this->params['url']['page_history_id'] ) ) );
            $page['Page'] = $history['PageHistory'];
        }

        // find any <element> tags within the content and extrapolate them out
        if( strstr( $page['Page']['content'], "<element" ) ) {
            App::Import( 'Helper', 'Content.SnippetElement' );
            $SnippetElement = new SnippetElementHelper();
            $models = $SnippetElement->element_models( $page['Page']['content'] );
            foreach( $models AS $model ) {
                $this->loadModel( $model );
            }
        }

        // find any <snippet> tags within the content and extrapolate them out
        if( strstr( $page['Page']['content'], "<snippet" ) ) {
            App::Import( 'Helper', 'Content.SnippetElement' );
            $SnippetElement = new SnippetElementHelper();
            $page['Page']['content'] = $SnippetElement->parse_snippet_tags( $page['Page']['content'] );
        }

        // what's the status of the page vs the user logged in status
        // is the user logged in?
        if( $user_status == 'logged-in' ) {

            // should they be allowed to see this page?
            if( $page['Page']['user_status'] == 'any' || $page['Page']['user_status'] == 'in' || $user_group == $page['Page']['user_status'] ) {
                $this->set( 'page', $page );

            // if not, redirect them to the unauthorized error page
            } else {
                $this->cakeError('error401');
                exit;
            }

        // the user is logged out
        } else {

            // should they be allowed to see this page?
            if( $page['Page']['user_status'] == 'any' || $page['Page']['user_status'] == 'out' ) {
                $this->set( 'page', $page );

            // if not, redirect them to the unauthorized error page
            } else {
                $this->cakeError('error401');
                exit;
            }
        }

		$layout = isset( $page['Layout']['file_name'] ) ? $page['Layout']['file_name'] : 'default';
        $this->layout = $layout;
	}


	function admin_index() {
		$this->disableCache();
        $this->Page->recursive = 1;

        $this->paginate['order'] = array( 'Page.name' );

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/pages/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
					"Page.name LIKE" 	   => "%" . $this->params['named']['q'] . "%",
					"Page.slug LIKE" 	   => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

        $scrollY = $this->Session->read( "History.Page.Sy" );
        $scrollY = $scrollY ? $scrollY : 0;
        $this->Session->delete( "History.Page.Sy" );
        $this->set( 'scrollY',   $scrollY );

		$this->set( 'pages', $this->paginate( 'Page' ) );
	}


	function admin_add() {
		$this->disableCache();
        if( strstr( $this->referer(), '/admin/pages/index' ) || $this->referer() == '/admin/pages/' || $this->referer() == '/admin/pages' ) {
			$this->Session->write( "History.Page.Add", $this->referer() );
            if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Page.Sy", $this->params['named']['sy'] ); }
		}

		if (!empty($this->data)) {
			$this->Page->create();

            App::import( 'Vendor', 'LayerCake.htmlcleaner' );
            $cleaner = new htmlcleaner;
            $this->data['Page']['content'] = $cleaner->clean_img_floats( $this->data['Page']['content'] );
            $this->data['Page']['content'] = $cleaner->clean_inline_styles( $this->data['Page']['content'] );

            // page starts as active
            $this->data['Page']['status']  = 'active';

			if( $this->Page->save($this->data) ) {

                // delete stored page slugs cache
                Cache::delete( 'page_slugs' );

				$this->Session->setFlash( 'The Page has been saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.Page.Add" );
				$this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash( 'The Page could not be saved', 'default', array('class' => 'error') );
			}
		}

        // load user groups
        $this->loadModel('UserGroup');
        $groups = $this->UserGroup->find( 'list', array( 'fields' => array('UserGroup.id', 'UserGroup.name') ) );
        $this->set( 'user_groups', $groups );

        // get slugs & links
        $this->loadModel('MenuItem');
        $page_slugs = $this->Page->query( "SELECT DISTINCT(`slug`) FROM `pages` WHERE `slug` LIKE '/%'" );
        $page_slugs = Set::extract($page_slugs, '{n}.pages.slug');
        $page_slugs = empty( $page_slugs[0] ) ? array() : $page_slugs;
        $menu_slugs = $this->MenuItem->query( "SELECT DISTINCT(`link`) FROM `menu_items` WHERE `link` LIKE '/%'" );
        $menu_slugs = Set::extract($menu_slugs, '{n}.menu_items.link');
        $menu_slugs = empty( $menu_slugs[0] ) ? array() : $menu_slugs;
        $this->set( 'menu_slugs', array_unique( array_merge( $page_slugs, $menu_slugs ) ) );

        $this->set( 'layouts', $this->Page->Layout->find('list') );
        $this->render( 'admin_form' );
	}


	function admin_edit( $id = null ) {
		$this->disableCache();
        if (!$id && empty($this->data)) {
            $this->Session->setFlash( 'Invalid Page', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

		if( strstr( $this->referer(), '/admin/pages/index' ) || $this->referer() == '/admin/pages/' || $this->referer() == '/admin/pages' ) {
			$this->Session->write( "History.Page.Edit." . $id, $this->referer() );
            if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Page.Sy", $this->params['named']['sy'] ); }
		}

		if (!empty($this->data)) {

            App::import( 'Vendor', 'LayerCake.htmlcleaner' );
            $cleaner = new htmlcleaner;
            $this->data['Page']['content'] = $cleaner->clean_img_floats( $this->data['Page']['content'] );
            $this->data['Page']['content'] = $cleaner->clean_inline_styles( $this->data['Page']['content'] );


			if( $this->Page->save($this->data) ) {

                // delete stored page slugs cache
                Cache::delete( 'page_slugs' );

                // clear view cache
                if( $this->data['Page']['slug'] == '/' ) {
                    clearCache( '/' );
                } else {
                    $cache_slug = '/' . str_replace( array('-', '/'), '_', ltrim( $this->data['Page']['slug'], '/' ) );
                    clearCache( $cache_slug );
                }

                $this->Session->setFlash( 'The Page has been saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.Page.Edit." . $id );
				$this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
			} else {
                $this->Session->setFlash( 'The Page could not be saved. Please, try again.', 'default', array('class' => 'error') );
			}
		}

		if (empty($this->data)) {
			$this->data = $this->Page->read(null, $id);
		}

        // load last 10 page histories
        $histories = $this->Page->PageHistory->find( 'all', array( 'conditions' => array( 'page_id' => $id ), 'limit' => 10, 'order' => array( 'PageHistory.id DESC' ) ) );
        $this->set( 'histories', $histories );

        // load user groups
        $this->loadModel('UserGroup');
        $groups = $this->UserGroup->find( 'list', array( 'fields' => array('UserGroup.id', 'UserGroup.name') ) );
        $this->set( 'user_groups', $groups );

        // get slugs & links
        $this->loadModel('MenuItem');
        $page_slugs = $this->Page->query( "SELECT DISTINCT(`slug`) FROM `pages` WHERE `slug` LIKE '/%'" );
        $page_slugs = Set::extract($page_slugs, '{n}.pages.slug');
        $page_slugs = empty( $page_slugs[0] ) ? array() : $page_slugs;
        $menu_slugs = $this->MenuItem->query( "SELECT DISTINCT(`link`) FROM `menu_items` WHERE `link` LIKE '/%'" );
        $menu_slugs = Set::extract($menu_slugs, '{n}.menu_items.link');
        $menu_slugs = empty( $menu_slugs[0] ) ? array() : $menu_slugs;
        $this->set( 'menu_slugs', array_unique( array_merge( $page_slugs, $menu_slugs ) ) );

        $this->set( 'layouts', $this->Page->Layout->find('list') );
        $this->render( 'admin_form' );
	}


	// save a preview version
    function admin_preview( $id = null ) {
        $this->disableCache();
        $this->autoRender = false;

        if( !empty($this->data) ) {
            App::import( 'Vendor', 'LayerCake.htmlcleaner' );
            $cleaner = new htmlcleaner;
            $this->data['Page']['content'] = $cleaner->clean_img_floats( $this->data['Page']['content'] );
            $this->data['Page']['content'] = $cleaner->clean_inline_styles( $this->data['Page']['content'] );

            $history = $this->data['Page'];
            $history['page_id'] = $this->data['Page']['id'];
            $history['page_id'] = empty( $history['page_id'] ) ? 0 : $history['page_id'];

            unset( $history['id'] );
            $this->Page->PageHistory->save( array( "PageHistory" => $history ), 0 );

            echo json_encode ( array( 'status' => 'success', 'id' => $this->Page->PageHistory->id ) );
		}
	}


    function admin_findedit() {
        $this->disableCache();

        // load this menu
        $menu_id = $this->params['named']['menu_id'];
        $this->loadModel('LayerCake.MenuItem');
        $menu_item = $this->MenuItem->find( 'first', array( 'conditions' => array( 'MenuItem.id' => $menu_id ) ) );
        if( $menu_item['MenuItem']['link'] == '/' ) {
            $menu_link = $menu_item['MenuItem']['link'];
        } else {
            $menu_link = preg_replace( "/\/$/", "", $menu_item['MenuItem']['link'] );
        }

        // does a page exist for this slug?
        $page = $this->Page->find( 'first', array( 'conditions' => array( 'slug' => $menu_link ) ) );

        // if a page exists, redirect to it
        if( isset( $page['Page']['id'] ) ) {

            $this->Session->write( "History.Page.Edit." . $page['Page']['id'], $this->referer() );
            if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] ); }
            $this->redirect( '/admin/pages/edit/' . $page['Page']['id'] );
            exit;

        // otherwise create it and then redirect to it
        } else {

            if(
                $this->Page->save( array( 'Page' => array(
                    'name'  => $menu_item['MenuItem']['title'],
                    'slug'  => $menu_link,
                    'title' => $menu_item['MenuItem']['title'],
                ) ) )
            ) {
                $page_id = $this->Page->id;
                $this->Session->write( "History.Page.Edit." . $page_id, $this->referer() );
                $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] );
                $this->redirect( '/admin/pages/edit/' . $page_id );
                exit;
            }
        }

        $this->redirect( '/admin/pages/' );
    }


    function admin_revert( $id = null ) {
        $this->disableCache();

        // load the previous page history
        $history = $this->Page->PageHistory->find( 'first', array( 'conditions' => array( 'PageHistory.id' => $id ) ) );
        $content = $history['PageHistory'];
        $content['id'] = $content['page_id'];
        unset( $content['page_id'] );

        // save old history
        $this->Page->save( array( "Page" => $content ) );

        // redirect back to edit page
        $this->redirect( '/admin/pages/edit/' . $content['id'] );
        exit;
    }


	function admin_delete($id = null) {
		$this->disableCache();

        if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}
		if ($this->Page->delete($id)) {
			if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Page.Sy", $this->params['named']['sy'] ); }
            $this->Session->setFlash( 'Deleted', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}


	function admin_status( $id = null ) {
		$this->disableCache();

        if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

        if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Page.Sy", $this->params['named']['sy'] ); }
        $new_status = isset( $this->params['named']['status'] ) ? $this->params['named']['status'] : 'active';
        $scope      = isset( $this->params['named']['scope']  ) ? $this->params['named']['scope']  : 'page';

		if( $this->Page->save( array( "Page" => array( 'id' => $id, 'status' => $new_status ) ) ) ) {

            // if the scope includes menu items as well
            if( $scope == 'page menu' ) {

                $this->loadModel( 'Content.MenuItem' );
                $page       = $this->Page->find( 'first',   array( 'conditions' => array( 'Page.id' => $id ) ) );
                $menu_items = $this->MenuItem->find( 'all', array( 'conditions' => array( 'link'    => $page['Page']['slug'] ) ) );

                foreach( $menu_items AS $item ) {
                    $this->MenuItem->create( false );
                    $this->MenuItem->save( array( "MenuItem" => array( 'id' => $item['MenuItem']['id'], 'status' => $new_status ) ) );

                    // delete any caches of this menu
                    Cache::delete( 'menus_' . $item['Menu']['slug'], 'default' );
                }
            }

            // delete stored page slugs cache
            Cache::delete( 'page_slugs' );

            $this->Session->setFlash( 'Status Updated', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}

} ?>