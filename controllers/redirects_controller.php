<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class RedirectsController extends LayerCakeAppController {

	var $name       = 'Redirects';
	var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle' );
    var $uses       = array( 'Content.Redirect' );

	function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'gotourl' );
	}


	function gotourl() {

        $url  = '/' . $this->params['url']['url'];
        $url  = preg_replace( '/\/$/', '', $url );

        // get the redirect
        $redirect = $this->Redirect->find( 'first', array(
            'conditions' => array(
                'slug'   => $url,
                'status' => 'active'
            )
        ));

        $this->redirect( $redirect['Redirect']['redirect_url'] );
        exit;
	}


	function admin_index() {
        $this->disableCache();
		$this->Redirect->recursive = 1;

        $this->paginate['order'] = array( 'Redirect.slug' );

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/redirects/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
					"Redirect.slug LIKE" 	        => "%" . $this->params['named']['q'] . "%",
					"Redirect.redirect_url LIKE" 	=> "%" . $this->params['named']['q'] . "%",
				)
			);
		}

        $scrollY = $this->Session->read( "History.Redirect.Sy" );
        $scrollY = $scrollY ? $scrollY : 0;
        $this->Session->delete( "History.Redirect.Sy" );
        $this->set( 'scrollY',   $scrollY );

		$this->set( 'redirects', $this->paginate( 'Redirect' ) );
	}


	function admin_add() {
		if( strstr( $this->referer(), '/admin/redirects/index' ) || $this->referer() == '/admin/redirects/' || $this->referer() == '/admin/redirects' ) {
			$this->Session->write( "History.Redirect.Add", $this->referer() );
            if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Redirect.Sy", $this->params['named']['sy'] ); }
		}

		if (!empty($this->data)) {
			$this->Redirect->create();

            // redirects starts as active
            $this->data['Redirect']['status']  = 'active';

			if( $this->Redirect->save($this->data) ) {

                // delete stored redirect slugs cache
                Cache::delete( 'redirect_slugs' );

				$this->Session->setFlash( 'The Redirect has been saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.Redirect.Add" );
				$this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash( 'The Redirect could not be saved', 'default', array('class' => 'error') );
			}
		}

        $this->render( 'admin_form' );
	}


	function admin_edit( $id = null ) {
		if (!$id && empty($this->data)) {
            $this->Session->setFlash( 'Invalid Redirect', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

		if( strstr( $this->referer(), '/admin/redirects/index' ) || $this->referer() == '/admin/redirects/' || $this->referer() == '/admin/redirects' ) {
			$this->Session->write( "History.Redirect.Edit." . $id, $this->referer() );
            if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Redirect.Sy", $this->params['named']['sy'] ); }
		}

		if (!empty($this->data)) {

			if( $this->Redirect->save($this->data) ) {

                // delete stored redirect slugs cache
                Cache::delete( 'redirect_slugs' );

                $this->Session->setFlash( 'The Redirect has been saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.Redirect.Edit." . $id );
				$this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
			} else {
                $this->Session->setFlash( 'The Redirect could not be saved. Please, try again.', 'default', array('class' => 'error') );
			}
		}

		if (empty($this->data)) {
			$this->data = $this->Redirect->read(null, $id);
		}

        $this->render( 'admin_form' );
	}


	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}
		if ($this->Redirect->delete($id)) {
			if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Redirect.Sy", $this->params['named']['sy'] ); }
            $this->Session->setFlash( 'Deleted', 'default', array('class' => 'success') );
            $this->redirect( $this->referer() );
		}
	}


	function admin_status( $id = null ) {
		if( !$id ) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

        if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.Redirect.Sy", $this->params['named']['sy'] ); }
        $new_status = isset( $this->params['named']['status'] ) ? $this->params['named']['status'] : 'active';

		if( $this->Redirect->save( array( "Redirect" => array( 'id' => $id, 'status' => $new_status ) ), false ) ) {

            // delete stored redirect slugs cache
            Cache::delete( 'redirect_slugs' );

            $this->Session->setFlash( 'Status Updated', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}


} ?>