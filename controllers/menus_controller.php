<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class MenusController extends LayerCakeAppController {

    var $uses     = array( "Content.Menu" );

	function admin_index() {
        $this->disableCache();
		$this->Menu->recursive = 1;

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/menus/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
					"Menu.title LIKE" 	   => "%" . $this->params['named']['q'] . "%",
					"Menu.slug LIKE" 	   => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

		$this->set( 'menus', $this->paginate( 'Menu' ) );
	}


	function admin_add() {
        $this->disableCache();
		if( strstr( $this->referer(), '/menus/index' ) || $this->referer() == '/admin/menus/' || $this->referer() == '/admin/menus' ) {
			$this->Session->write( "History.Menu.Add", $this->referer() );
		}

		if (!empty($this->data)) {
			$this->Menu->create();

			if ($this->Menu->save($this->data)) {

                // delete any caches of this menu
                $this_menu = $this->Menu->findById( $this->Menu->id );
                Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

				$this->Session->setFlash( 'Saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.Menu.Add" );
				$this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash( 'Could not save, please try again', 'default', array('class' => 'error') );
			}
		}

        $this->render( 'admin_form' );
	}


	function admin_edit($id = null) {
        $this->disableCache();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

		if( strstr( $this->referer(), '/menus/index' ) || $this->referer() == '/admin/menus/' || $this->referer() == '/admin/menus' ) {
			$this->Session->write( "History.Menu.Edit." . $id, $this->referer() );
		}

		if (!empty($this->data)) {

			if ($this->Menu->save($this->data)) {

                // delete any caches of this menu
                $this_menu = $this->Menu->findById( $id );
                Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

                $this->Session->setFlash( 'Saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.Menu.Edit." . $id );
				$this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
			} else {
                $this->Session->setFlash( 'Could not save. Please, try again.', 'default', array('class' => 'error') );
			}
		}

		if (empty($this->data)) {
			$this->data = $this->Menu->read(null, $id);
		}

        $this->render( 'admin_form' );
	}


	function admin_delete($id = null) {
        $this->disableCache();
		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

        // delete any caches of this menu
        $this_menu = $this->Menu->findById( $id );
        Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

		if ($this->Menu->delete($id)) {
			$this->Session->setFlash( 'Deleted', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}

    // ajax requests
    //******************************************************************************
    // get the menu
    function admin_ajax_menu( $menu_id = null ) {
        if( !$menu_id ) { exit; }
        $this->Menu->recursive = -1;
        $menu = $this->Menu->findById( $menu_id );
        echo json_encode( $menu, JSON_NUMERIC_CHECK  );
        exit;
    }

} ?>
