<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class MenuItemsController extends LayerCakeAppController {

    var $uses     = array( "Content.MenuItem" );

    private function check_menu_id() {
        $menu_id = isset( $this->params['named']['menu_id'] ) ? $this->params['named']['menu_id'] : $this->data['MenuItem']['menu_id'];
        if( !isset( $menu_id ) || ! is_numeric( $menu_id ) ) {
            $this->Session->setFlash( 'You must pick a menu first', 'default', array('class' => 'warning') );
			$this->redirect( "/admin/menus/" );
        }

        return $menu_id;
    }


	function admin_index() {
        $this->disableCache();
		$menu_id = $this->check_menu_id();
		$this->MenuItem->recursive = 1;
        $this->MenuItem->addScope( $menu_id );

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/menu_items/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
					"MenuItem.title LIKE" 	   => "%" . $this->params['named']['q'] . "%",
					"MenuItem.slug LIKE" 	   => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

        $scrollY = $this->Session->read( "History.MenuItem.Sy" );
        $scrollY = $scrollY ? $scrollY : 0;
        $this->Session->delete( "History.MenuItem.Sy" );

        $this->set( 'menuItems', $this->MenuItem->find( 'threaded', array( 'order' => array( 'MenuItem.lft ASC'), 'conditions' => array( 'MenuItem.menu_id' => $menu_id ) ) ) );
        $this->set( 'scrollY',   $scrollY );
	}


    function admin_add() {
        $this->disableCache();
		$menu_id = $this->check_menu_id();
        $this->MenuItem->addScope( $menu_id );

		if( strstr( $this->referer(), '/menu_items/index' ) ) {
			$this->Session->write( "History.MenuItem.Add", $this->referer() );
            if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] ); }
		}

		if (!empty($this->data)) {
			$this->MenuItem->create();

            // items starts as active
            $this->data['MenuItem']['status']  = 'active';

			if ($this->MenuItem->save($this->data)) {

                // delete any caches of this menu
                $this_menu = $this->MenuItem->findById( $this->MenuItem->id );
                Cache::delete( 'menus_' . $this_menu['Menu']['slug'] );

				$this->Session->setFlash( 'Saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.MenuItem.Add" );
				$this->redirect( isset( $history ) ? $history : '/admin/menu_items/index/menu_id:' . $menu_id );
			} else {
				$this->Session->setFlash( 'Could not save, please try again', 'default', array('class' => 'error') );
			}
		}

        // load user groups
        $this->loadModel('UserGroup');
        $groups = $this->UserGroup->find( 'list', array( 'fields' => array('UserGroup.id', 'UserGroup.name') ) );
        $this->set( 'user_groups', $groups );

        // get slugs & links
        $this->loadModel('Page');
        $page_slugs = $this->Page->query( "SELECT DISTINCT(`slug`) FROM `pages` WHERE `slug` LIKE '/%'" );
        $page_slugs = Set::extract($page_slugs, '{n}.pages.slug');
        $page_slugs = empty( $page_slugs[0] ) ? array() : $page_slugs;
        $menu_slugs = $this->MenuItem->query( "SELECT DISTINCT(`link`) FROM `menu_items` WHERE `link` LIKE '/%'" );
        $menu_slugs = Set::extract($menu_slugs, '{n}.menu_items.link');
        $menu_slugs = empty( $menu_slugs[0] ) ? array() : $menu_slugs;
        $this->set( 'menu_slugs', array_unique( array_merge( $page_slugs, $menu_slugs ) ) );

		$this->set( 'menus',     $this->MenuItem->Menu->find( 'list', array( 'recursive' => 0 ) ) );
		$this->set( 'menuItems', $this->MenuItem->find( 'threaded', array( 'conditions' => array( 'MenuItem.menu_id' => $menu_id ), 'order' => array( 'MenuItem.lft ASC'),  ) ) );
        $this->render( 'admin_form' );
	}


	function admin_edit($id = null) {
        $this->disableCache();
		$menu_id = $this->check_menu_id();
        $this->MenuItem->addScope( $menu_id );

		if (!$id && empty($this->data)) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

		if( strstr( $this->referer(), '/menu_items/index' ) ) {
			$this->Session->write( "History.MenuItem.Edit." . $id, $this->referer() );
            if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] ); }
		}

		if (!empty($this->data)) {

            // get the menu item before we save it
            $menuitem = $this->MenuItem->findById( $id );

            if ($this->MenuItem->save($this->data)) {

                // reorder the menu the page was moved from
                if( $menuitem['MenuItem']['menu_id'] != $this->data['MenuItem']['menu_id'] ) {
                    // if there was an error with the move, try to fix the tree
                    $this->MenuItem->recover( 'parent' );
                    $this->MenuItem->recover( 'tree ');

                    $this->MenuItem->addScope( $menuitem['MenuItem']['menu_id'] );
                    $this->MenuItem->recover( 'parent' );
                    $this->MenuItem->recover( 'tree ');
                }

                // if the menu item slug changed
                if( $menuitem['MenuItem']['link'] != $this->data['MenuItem']['link'] ) {
                    $this->loadModel( 'Content.Page' );
                    $page = $this->Page->find( 'first',   array( 'conditions' => array( 'slug' => $menuitem['MenuItem']['link'] ) ) );
                    if( !empty( $page ) ) {
                        $this->Page->save( array( "Page" => array( 'id' => $page['Page']['id'], 'slug' => $this->data['MenuItem']['link'] ) ) );
                    }

                    // delete stored page slugs cache
                    Cache::delete( 'page_slugs' );
                }

                // delete any caches of this menu
                $this_menu = $this->MenuItem->findById( $id );
                Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

                $this->Session->setFlash( 'Saved', 'default', array('class' => 'success') );
				$history  = $this->Session->read( "History.MenuItem.Edit." . $id );
				$this->redirect( isset( $history ) ? $history : '/admin/menu_items/index/menu_id:' . $menu_id );
			} else {
                $this->Session->setFlash( 'Could not save. Please, try again.', 'default', array( 'class' => 'error' ) );
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MenuItem->read(null, $id);
		}

        // load user groups
        $this->loadModel('UserGroup');
        $groups = $this->UserGroup->find( 'list', array( 'fields' => array('UserGroup.id', 'UserGroup.name') ) );
        $this->set( 'user_groups', $groups );

        // get slugs & links
        $this->loadModel('Page');
        $page_slugs = $this->Page->query( "SELECT DISTINCT(`slug`) FROM `pages` WHERE `slug` LIKE '/%'" );
        $page_slugs = Set::extract($page_slugs, '{n}.pages.slug');
        $page_slugs = empty( $page_slugs[0] ) ? array() : $page_slugs;
        $menu_slugs = $this->MenuItem->query( "SELECT DISTINCT(`link`) FROM `menu_items` WHERE `link` LIKE '/%'" );
        $menu_slugs = Set::extract($menu_slugs, '{n}.menu_items.link');
        $menu_slugs = empty( $menu_slugs[0] ) ? array() : $menu_slugs;
        $this->set( 'menu_slugs', array_unique( array_merge( $page_slugs, $menu_slugs ) ) );

		$this->set( 'menus',     $this->MenuItem->Menu->find( 'list', array( 'recursive' => 0 ) ) );
		$this->set( 'menuItems', $this->MenuItem->find( 'threaded', array( 'conditions' => array( 'MenuItem.menu_id' => $menu_id ), 'order' => array( 'MenuItem.lft ASC'),  ) ) );
        $this->render( 'admin_form' );
	}


	function admin_delete($id = null) {
        $this->disableCache();
		$menu_id = $this->check_menu_id();
        $this->MenuItem->addScope( $menu_id );

		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

        // delete any caches of this menu
        $this_menu = $this->MenuItem->findById( $id );
        Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

        if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] ); }

		if ($this->MenuItem->delete($id)) {
			$this->Session->setFlash( 'Deleted', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}


	function admin_moveup( $id = null ) {
        $this->disableCache();
		$menu_id = $this->check_menu_id();
        $this->MenuItem->addScope( $menu_id );

		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

        // delete any caches of this menu
        $this_menu = $this->MenuItem->findById( $id );
        Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

        if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] ); }

		// try to move the page up
        if( $this->MenuItem->moveUp( $id, 1 ) ) {
			$this->Session->setFlash( 'Moved Up', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		} else {

            // if there was an error with the move, try to fix the tree
            $this->MenuItem->recover( 'parent' );
            $this->MenuItem->recover( 'tree ');

            // then try the move again
            if( $this->MenuItem->moveUp( $id, 1 ) ) {
                $this->Session->setFlash( 'Moved Up', 'default', array('class' => 'success') );
                $this->redirect( $this->referer() );
            } else {
                $this->Session->setFlash( 'There was an error moving this page. Please contact the site administrator', 'default', array('class' => 'error') );
    			$this->redirect( $this->referer() );
            }
        }
	}


	function admin_movedown( $id = null ) {
        $this->disableCache();
		$menu_id = $this->check_menu_id();
        $this->MenuItem->addScope( $menu_id );

		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

        // delete any caches of this menu
        $this_menu = $this->MenuItem->findById( $id );
        Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

        if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] ); }

		// try to move the page down
        if( $this->MenuItem->moveDown( $id, 1 ) ) {
			$this->Session->setFlash( 'Moved Up', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		} else {

            // if there was an error with the move, try to fix the tree
            $this->MenuItem->recover( 'parent' );
            $this->MenuItem->recover( 'tree ');

            // then try the move again
            if( $this->MenuItem->moveDown( $id, 1 ) ) {
                $this->Session->setFlash( 'Moved Up', 'default', array('class' => 'success') );
                $this->redirect( $this->referer() );
            } else {
                $this->Session->setFlash( 'There was an error moving this page. Please contact the site administrator', 'default', array('class' => 'error') );
    			$this->redirect( $this->referer() );
            }
        }
	}


	function admin_status( $id = null ) {
        $this->disableCache();
		$menu_id = $this->check_menu_id();
        $this->MenuItem->addScope( $menu_id );

		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

        // delete any caches of this menu
        $this_menu = $this->MenuItem->findById( $id );
        Cache::delete( 'menus_' . $this_menu['Menu']['slug'], 'default' );

        if( isset( $this->params['named']['sy'] ) ) { $this->Session->write( "History.MenuItem.Sy", $this->params['named']['sy'] ); }
        $new_status = isset( $this->params['named']['status'] ) ? $this->params['named']['status'] : 'active';
        $scope      = isset( $this->params['named']['scope']  ) ? $this->params['named']['scope']  : 'menu';

		if( $this->MenuItem->save( array( "MenuItem" => array( 'id' => $id, 'status' => $new_status ) ) ) ) {

            // if the scope includes page content as well
            if( $scope == 'menu page' ) {
                $this->loadModel( 'Content.Page' );
                $menu_item = $this->MenuItem->find( 'first', array( 'conditions' => array( 'MenuItem.id' => $id ) ) );
                $page      = $this->Page->find( 'first',   array( 'conditions' => array( 'slug'    => $menu_item['MenuItem']['link'] ) ) );
                $this->Page->save( array( "Page" => array( 'id' => $page['Page']['id'], 'status' => $new_status ) ) );

                // delete stored page slugs cache
                Cache::delete( 'page_slugs' );
            }

            $this->Session->setFlash( 'Status Updated', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}

    // ajax requests
    //******************************************************************************
    // get a list of menuitems based on a menu id
    function admin_ajax_menu_items( $menu_id = null ) {
        if( !$menu_id ) { exit; }
        $menuItems = $this->MenuItem->find( 'threaded', array( 'conditions' => array( 'MenuItem.menu_id' => $menu_id ), 'order' => array( 'MenuItem.lft ASC') ) );

        $parents = array(
            array( 'id' => '0',  'title' => 'Menu Root' ),
            array( 'id' => '',   'title' => '------------------------------------------' )
        );

        for( $i=0; $i < count( $menuItems ); $i++ ) {
            $menuItem = $menuItems[$i];

            if( isset( $menuItem['MenuItem']['depth'] ) ){
                $menuItem['MenuItem']['title'] = str_repeat( '&nbsp;', 4 ) . str_repeat( '&mdash;', $menuItem['MenuItem']['depth'] ) . '&nbsp;&nbsp;' . $menuItem['MenuItem']['title'];
            } else {
                $menuItem['MenuItem']['title'] = '&nbsp;&nbsp;&nbsp;' . $menuItem['MenuItem']['title'];
            }

            if( count( $menuItem['children'] ) >= 1 ) {
                foreach( $menuItem['children'] AS &$child ) {
                    $child['MenuItem']['depth'] = isset( $menuItem['MenuItem']['depth'] ) ? $menuItem['MenuItem']['depth'] + 1 : 1;
                }

                array_splice( $menuItems, $i+1, 0, $menuItem['children'] );
            }

            if( $menuItem['MenuItem']['id'] != $this->data['MenuItem']['id'] ) {
                $parents[] = array( 'id' => $menuItem['MenuItem']['id'], 'title' => $menuItem['MenuItem']['title'] );
            }
        }

        echo json_encode( $parents, JSON_NUMERIC_CHECK  );
        exit;
    }

    // get the menu item
    function admin_ajax_menu_item( $menu_item_id = null ) {
        if( !$menu_item_id ) { exit; }
        $this->MenuItem->recursive = -1;
        $menuitem = $this->MenuItem->findById( $menu_item_id );
        echo json_encode( $menuitem, JSON_NUMERIC_CHECK  );
        exit;
    }
}