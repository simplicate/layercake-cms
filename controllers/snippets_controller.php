<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class SnippetsController extends LayerCakeAppController {

	function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'view' );
	}

    var $paginate = array( 'order' => array( 'Snippet.location ASC', 'Snippet.name ASC' ) );
    var $uses     = array( "Content.Snippet" );


	function view() {

        $url  = '/' . $this->params['url']['url'];
        $url  = preg_replace( '/\/$/', '', $url );

        $snippet = $this->Snippet->findBySlug( $url );

		if ( !$snippet ) {
			$snippet['Snippet']['name']    = $url;
            $snippet['Snippet']['content'] = '';
		}

        if( $url == '/' ) {
            $this->layout = 'home';
        }

		$this->set( 'snippet', $snippet );
	}


	function admin_index() {
        $this->disableCache();
		$this->Snippet->recursive = 1;

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/snippets/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
					"Snippet.name LIKE" 	=> "%" . $this->params['named']['q'] . "%",
					"Snippet.location LIKE" => "%" . $this->params['named']['q'] . "%",
					"Snippet.content LIKE"  => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

		$this->set( 'snippets', $this->paginate( 'Snippet' ) );
	}


	function admin_add() {
        $this->disableCache();
		if( strstr( $this->referer(), '/admin/snippets/index' ) || $this->referer() == '/admin/snippets/' || $this->referer() == '/admin/snippets' ) {
			$this->Session->write( "History.Snippet.Add", $this->referer() );
		}

		if( !empty($this->data) ) {
			$this->Snippet->create();
            $this->admin_save();
		}

        // load user groups
        $this->loadModel('UserGroup');
        $groups = $this->UserGroup->find( 'list', array( 'fields' => array('UserGroup.id', 'UserGroup.name') ) );
        $this->set( 'user_groups', $groups );

        // add layout locations
        $this->loadModel( "Content.Layout" );
        $this->set( 'snippet_locations', $this->Layout->snippet_locations() );
        $this->set( 'snippet_templates', $this->Snippet->SnippetTemplate->find( 'list' ) );

        $this->render( 'admin_form' );
	}


	function admin_edit( $id = null ) {
        $this->disableCache();
		if (!$id && empty($this->data)) {
            $this->Session->setFlash( 'Invalid Snippet', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

		if( strstr( $this->referer(), '/admin/snippets/index' ) || $this->referer() == '/admin/snippets/' || $this->referer() == '/admin/snippets' ) {
			$this->Session->write( "History.Snippet.Edit." . $id, $this->referer() );
		}

		if( !empty($this->data) ) {
            $this->admin_save( $id );
		} else {
            $this->data = $this->Snippet->read( null, $id );
        }

        $snippet = $this->Snippet->find( 'first', array( 'conditions' => array( 'Snippet.id' => $id ) ) );

        // load user groups
        $this->loadModel('UserGroup');
        $groups = $this->UserGroup->find( 'list', array( 'fields' => array('UserGroup.id', 'UserGroup.name') ) );
        $this->set( 'user_groups', $groups );

        // add layout locations
        $this->loadModel( "Content.Layout" );
        $this->set( 'snippet_locations', $this->Layout->snippet_locations() );
        $this->set( 'snippet_templates', $this->Snippet->SnippetTemplate->find( 'list' ) );

        $this->render( 'admin_form' );
	}


    private function admin_save( $id = null ) {
        $this->disableCache();

        App::import( 'Vendor', 'LayerCake.htmlcleaner' );
        $cleaner = new htmlcleaner;
        $this->data['Snippet']['content'] = $cleaner->clean_img_floats( $this->data['Snippet']['content'] );
        $this->data['Snippet']['content'] = $cleaner->clean_inline_styles( $this->data['Snippet']['content'] );

        if( $this->Snippet->save($this->data) ) {

            $this->Snippet->SnippetUrl->deleteAll( array( 'snippet_id' => $this->Snippet->id ) );

            $include_str = preg_replace( "/\r/", "", $this->data['Snippet']['UrlsInclude'] );
            foreach( explode("\n", $include_str) as $url ) {
                $url = trim( $url );
                if( empty( $url ) ) { continue; }
                $urls[] = array( 'SnippetUrl' => array( 'snippet_id' => $this->Snippet->id, 'url' => $url, 'type' => 'include' ) );
            }

            $exclude_str = preg_replace( "/\r/", "", $this->data['Snippet']['UrlsExclude'] );
            foreach( explode("\n", $exclude_str) as $url ) {
                $url = trim( $url );
                if( empty( $url ) ) { continue; }
                $urls[] = array( 'SnippetUrl' => array( 'snippet_id' => $this->Snippet->id, 'url' => $url, 'type' => 'exclude' ) );
            }

            $this->Snippet->SnippetUrl->saveAll( $urls );

            $this->Session->setFlash( 'The Snippet has been saved', 'default', array('class' => 'success') );
            $history = ( isset( $id ) ) ? $this->Session->read( "History.Snippet.Edit." . $id ) : $this->Session->read( "History.Snippet.Add" );
            $this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
        } else {
            $this->Session->setFlash( 'The Snippet could not be saved. Please, try again.', 'default', array('class' => 'error') );
        }
    }


	function admin_delete($id = null) {
        $this->disableCache();
		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}
		if ($this->Snippet->delete($id)) {
			$this->Session->setFlash( 'Deleted', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}

} ?>