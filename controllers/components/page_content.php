<?php

class PageContentComponent extends Object {

    // called before Controller::beforeFilter()
    function initialize( &$controller, $settings = array() ) {
        // saving the controller reference for later use
        $this->controller =& $controller;
    }

    function load_for_this_url() {

        $this->controller->loadModel( 'Content.Page' );

        $url  = '/' . $this->controller->params['url']['url'];
        $url  = preg_replace( '/\/$/', '', $url );

        // remove /index/page:1/ etc
        if( stristr( $url, '/index/' ) && stristr( $url, 'page:' ) ) {
            $start = stripos( $url, '/index/' );
            $url = substr( $url, 0, $start );
        }

        // get the page
        $page = $this->controller->Page->find( 'first', array(
            'conditions' => array(
                'slug'   => $url,
                'status' => 'active'
            ),
            'contain' => array( 'Layout' )
        ));

        return $page;
    }
}