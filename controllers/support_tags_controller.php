<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class SupportTagsController extends LayerCakeAppController {

    var $paginate = array( 'order' => array( 'SupportTag.location ASC', 'SupportTag.name ASC' ) );
    var $uses     = array( "Content.SupportTag" );

	function admin_index() {
        $this->disableCache();
		$this->SupportTag->recursive = 1;

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/support_tags/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
					"SupportTag.name LIKE" 	=> "%" . $this->params['named']['q'] . "%",
					"SupportTag.location LIKE" => "%" . $this->params['named']['q'] . "%",
					"SupportTag.content LIKE"  => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

		$this->set( 'support_tags', $this->paginate( 'SupportTag' ) );
	}


	function admin_add() {
        $this->disableCache();
		if( strstr( $this->referer(), '/admin/support_tags/index' ) || $this->referer() == '/admin/support_tags/' || $this->referer() == '/admin/support_tags' ) {
			$this->Session->write( "History.SupportTag.Add", $this->referer() );
		}

		if( !empty($this->data) ) {
			$this->SupportTag->create();
            $this->admin_save();
		}

        $this->render( 'admin_form' );
	}


	function admin_edit( $id = null ) {
        $this->disableCache();
		if (!$id && empty($this->data)) {
            $this->Session->setFlash( 'Invalid Support Tag', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

		if( strstr( $this->referer(), '/admin/support_tags/index' ) || $this->referer() == '/admin/support_tags/' || $this->referer() == '/admin/support_tags' ) {
			$this->Session->write( "History.SupportTag.Edit." . $id, $this->referer() );
		}

		if( !empty($this->data) ) {
            $this->admin_save( $id );
		} else {
            $this->data = $this->SupportTag->read( null, $id );
        }

        $this->render( 'admin_form' );
	}


    private function admin_save( $id = null ) {
        $this->disableCache();

        if( $this->SupportTag->save($this->data) ) {

            $this->SupportTag->SupportTagUrl->deleteAll( array( 'support_tag_id' => $this->SupportTag->id ) );

            $include_str = preg_replace( "/\r/", "", $this->data['SupportTag']['UrlsInclude'] );
            foreach( explode("\n", $include_str) as $url ) {
                $url = trim( $url );
                if( empty( $url ) ) { continue; }
                $urls[] = array( 'SupportTagUrl' => array( 'support_tag_id' => $this->SupportTag->id, 'url' => $url, 'type' => 'include' ) );
            }

            $exclude_str = preg_replace( "/\r/", "", $this->data['SupportTag']['UrlsExclude'] );
            foreach( explode("\n", $exclude_str) as $url ) {
                $url = trim( $url );
                if( empty( $url ) ) { continue; }
                $urls[] = array( 'SupportTagUrl' => array( 'support_tag_id' => $this->SupportTag->id, 'url' => $url, 'type' => 'exclude' ) );
            }

            $this->SupportTag->SupportTagUrl->saveAll( $urls );

            $this->Session->setFlash( 'The Support Tag has been saved', 'default', array('class' => 'success') );
            $history = ( isset( $id ) ) ? $this->Session->read( "History.SupportTag.Edit." . $id ) : $this->Session->read( "History.SupportTag.Add" );
            $this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );
        } else {
            $this->Session->setFlash( 'The Support Tag could not be saved. Please, try again.', 'default', array('class' => 'error') );
        }
    }


	function admin_delete($id = null) {
        $this->disableCache();
		if (!$id) {
			$this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}
		if ($this->SupportTag->delete($id)) {
			$this->Session->setFlash( 'Deleted', 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
	}

    function admin_status( $id = null ) {
        $this->disableCache();

        if (!$id) {
            $this->Session->setFlash( 'Invalid ID', 'default', array('class' => 'error') );
            $this->redirect( $this->referer() );
        }

        $new_status = isset( $this->params['named']['status'] ) ? $this->params['named']['status'] : 'active';

        if( $this->SupportTag->save( array( "SupportTag" => array( 'id' => $id, 'status' => $new_status ) ) ) ) {
            $this->Session->setFlash( 'Status Updated', 'default', array('class' => 'success') );
            $this->redirect( $this->referer() );
        }
    }

} ?>