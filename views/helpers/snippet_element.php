<?php class SnippetElementHelper extends AppHelper {

    function element_models( $html ) {

        // match all element tags
        preg_match_all( '/<element\s*([^>]*)\s*\/?>/', $html, $customTags, PREG_SET_ORDER );

        $models = array();

        foreach( $customTags as $customTag ) {
            $originalTag    = $customTag[0];
            $rawAttributes  = $customTag[1];

            preg_match_all( '/([^=\s]+)="([^"]+)"/', $rawAttributes, $attributes, PREG_SET_ORDER );

            $attr = array();

            foreach( $attributes as $attribute ) {
                $name  = $attribute[1];
                $value = $attribute[2];

                $attr[$name] = $value;
            }

            if( isset($attr['plugin']) && isset($attr['model']) ) {
                $models[] = $attr['plugin'] . '.' . $attr['model'];
            }
        }

        return $models;
    }


    function parse_element_tags( $html ) {

        // match all element tags
        preg_match_all( '/<element\s*([^>]*)\s*\/?>/', $html, $customTags, PREG_SET_ORDER );

        foreach( $customTags as $customTag ) {
            $originalTag    = $customTag[0];
            $rawAttributes  = $customTag[1];

            preg_match_all( '/([^=\s]+)="([^"]+)"/', $rawAttributes, $attributes, PREG_SET_ORDER );

            $attr = array();

            foreach( $attributes as $attribute ) {
                $name  = $attribute[1];
                $value = $attribute[2];

                $attr[$name] = $value;
            }

            $View         =& ClassRegistry::getObject('view');
            $element_html = $View->element( $attr['name'], $attr  );
            $html         = str_replace( $originalTag, $element_html, $html );
            return $html;
        }
    }


    function parse_snippet_tags( $html ) {

        // match all element tags
        preg_match_all( '/<snippet\s*([^>]*)\s*\/?>/', $html, $customTags, PREG_SET_ORDER );

        $html         = str_replace( "<p><snippet",    "<snippet",   $html );
        $html         = str_replace( "</snippet></p>", "</snippet>", $html );

        foreach( $customTags as $customTag ) {
            $originalTag    = $customTag[0];
            $rawAttributes  = $customTag[1];

            preg_match_all( '/([^=\s]+)="([^"]+)"/', $rawAttributes, $attributes, PREG_SET_ORDER );

            $attr = array();

            foreach( $attributes as $attribute ) {
                $name  = $attribute[1];
                $value = $attribute[2];

                $attr[$name] = $value;
            }

            // find condtions for the snippet
            $find_conditions = array(
                'Snippet.active'    => 1,
                'Snippet.name'      => $attr['name']
            );

            // only load the snippets that should be loaded by user status
            $user_status = isset( $_SESSION['User']['User']['id'] ) ? 'logged-in' : 'logged-out';
            if( $user_status == 'logged-out' ) {
                $find_conditions['user_status'] = array( 'any', 'out' );
            } else {

                $user_statuses = array( 'any', 'in' );

                if( isset( $_SESSION['User']['UserGroup']['id'] ) ) {
                    $user_statuses[] =$_SESSION['User']['UserGroup']['id'];
                }

                $find_conditions['user_status'] = $user_statuses;
            }

            // load snippet
            App::import( 'Model', 'Content.Snippet' );
            $Snippet      = new Snippet;
            $this_snippet = $Snippet->find( 'first', array( 'conditions' => $find_conditions ) );
            $new_content  = isset( $this_snippet['Snippet']['content'] ) ? $this_snippet['Snippet']['content'] : '';
            $html         = str_replace( $originalTag, $new_content, $html );

            return $html;
        }
    }
} ?>