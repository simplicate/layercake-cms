<h1>Menu Settings</h1>

<?= $form->create( 'Menu' ); ?>
<?= $form->input( 'id' );    ?>

<div class="form-left">
    <?= $form->input( 'title', array( 'label' => 'Name' ) ); ?>
</div>

<div class="form-right" style="background:#eee;">
    <div style="padding: 20px 20px 0 20px;">
        <div class="form-left">
            <?= $form->input( 'slug', array( 'label' => 'Slug' ) );  ?>
        </div>

        <div class="form-right">
            <?= $form->input( 'language', array( 'style'=> 'width: 100%; margin: 0;', 'type' => 'select', 'empty' => true, 'options' => array( 'eng' => 'English', 'fre' => 'French' ) ) ); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>

<script type="text/javascript">
    $(document).ready(function() {
        // slugify
        $('#MenuTitle').change( function() {
            if( $('#MenuSlug').val() == '' || $('#MenuSlug').val() == '/' ) {
                slug = $('#MenuTitle').val();
                slug = slug.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
                slug = slug.replace(/-/gi, "_");
                slug = slug.replace(/\s/gi, "-");

                $('#MenuSlug').val( slug.toLowerCase() );
            }
        });

        $('#MenuItemMenuId').change( function() {
            updateMenuItems();
        });
    });
</script>

<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link', array( "plugin" => "admin", "HistoryModel" => "Menu" ) ); ?>