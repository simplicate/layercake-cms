<h1>Menus</h1>

<? if( sizeof( $menus ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array( 'action' => 'index' ) ); ?></p>
<? endif; ?>

<?= $html->link( "New Menu", '/admin/menus/add', array( 'class' => 'button', 'escape' => false )  ); ?>

<? if( sizeof( $menus ) >= 1 ): ?>

    <?= $paginator->options( array( 'url' => $this->passedArgs ) ); ?>

    <table class="list">
        <tr>
            <th><?= $paginator->sort('title');?></th>
            <th><?= $paginator->sort('slug' );?></th>
            <th><?= $paginator->sort('language');?></th>
            <th>Edit Menu Items</th>
            <th class="t-center">Actions</th>
        </tr>

        <tbody>
            <? foreach( $menus as $menu ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td><?= $menu['Menu']['title']; ?></td>
                    <td><?= $menu['Menu']['slug']; ?></td>
                    <td><?= $menu['Menu']['language']; ?></td>
                    <td>
                        <?= $html->link( 'Menu Items', '/admin/menu_items/index/menu_id:' . $menu['Menu']['id'] ); ?>
                    </td>
                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"   />', array( 'action' => 'edit',   $menu['Menu']['id']), array( 'escape' => false ) ); ?>
                        |
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $menu['Menu']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $menu['Menu']['title'] ) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>


    <? if( $this->params['paging']['Menu']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers();		 ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? endif; ?>