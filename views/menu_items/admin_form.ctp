<h1>Menu Item Settings</h1>

<?
    $parents = array( '0' => 'Menu Root', '' => '------------------------------------------' );
    for( $i=0; $i < count( $menuItems ); $i++ ) {
        $menuItem = $menuItems[$i];

        if( isset( $menuItem['MenuItem']['depth'] ) ){
            $menuItem['MenuItem']['title'] = str_repeat( '&nbsp;', 4 ) . str_repeat( '&mdash;', $menuItem['MenuItem']['depth'] ) . '&nbsp;&nbsp;' . $menuItem['MenuItem']['title'];
        } else {
            $menuItem['MenuItem']['title'] = '&nbsp;&nbsp;&nbsp;' . $menuItem['MenuItem']['title'];
        }

        if( count( $menuItem['children'] ) >= 1 ) {
            foreach( $menuItem['children'] AS &$child ) {
                $child['MenuItem']['depth'] = isset( $menuItem['MenuItem']['depth'] ) ? $menuItem['MenuItem']['depth'] + 1 : 1;
            }

            array_splice( $menuItems, $i+1, 0, $menuItem['children'] );
        }

        if( $menuItem['MenuItem']['id'] != $this->data['MenuItem']['id'] ) {
            $parents[$menuItem['MenuItem']['id']] = $menuItem['MenuItem']['title'];
        }
    }
?>

<?= $form->create( 'MenuItem' );?>
<?= $form->input( 'id' ); ?>

<div class="form-left">
    <?= $form->input( 'title' ); ?>
    <?= $form->input( 'link'  );  ?>
    <?= $form->input( 'description' ); ?>
</div>

<div class="form-right" style="background:#eee;">
    <div style="padding: 20px 20px 0 20px;">

        <div class="form-left">
            <?= $form->input( 'menu_id', array( 'label' => 'Parent Menu', 'style' => 'width:100%;', 'type' => 'select', 'options' => $menus, 'default' => isset( $this->passedArgs['menu_id'] ) ? $this->passedArgs['menu_id'] : ''  ) ); ?>

            <?
                $user_status = array( 'any' => 'All Users', 'out' => 'Logged Out Users', 'in' => 'Logged In Users', '' => "----------" );
                foreach( $user_groups AS $key => $name ) { $user_status[$key] = $name; }
            ?>
            <?= $form->input( 'user_status',    array( 'label' => 'Show for Which Users', 'style' => 'width:100%;', 'type' => 'select', 'options' => $user_status ) ); ?>
        </div>

        <div class="form-right">
            <?= $form->input( 'old_parent_id',  array( 'type' => 'hidden', 'value' => $this->data['MenuItem']['parent_id'] ) ); ?>
            <?= $form->input( 'parent_id',      array( 'label' => 'Parent Menu Item', 'style' => 'width:100%;', 'type' => 'select', 'options' => $parents, 'escape' => false ) ); ?>

            <?= $form->input( 'hide_in_menu',   array( 'label' => 'Display in Navigation?', 'style' => 'width:100%;', 'type' => 'select', 'options' => array( '' => 'Yes', '1' => 'No, Hide in Navigation' ), 'escape' => false ) ); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>

<script type="text/javascript">

    $(document).ready(function() {

        $('#MenuItemMenuId').change( function() {
            updateMenuItems();
            getMenu();
        });

        // slugify
        $('#MenuItemTitle').change( function() {
            if( $('#MenuItemLink').val() == '' || $('#MenuItemLink').val() == '/' ) {
                slug   = $('#MenuItemTitle').val();
                slug   = slug.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
                slug   = slug.replace(/-/gi, "_");
                slug   = slug.replace(/\s/gi, "-");
                console.log( menuData );
                prefix = ( menuData.Menu.language.length >= 1 ) ? '/' + menuData.Menu.language : '';
                $('#MenuItemLink').val( prefix + '/' + slug.toLowerCase() );
            }
        });

        // slugify when changing parent
        $('#MenuItemParentId').change( function() {
            if( humanChangedSlug == false ) {
                $.ajax({
                    type: "GET",
                    url: '/admin/menu_items/ajax_menu_item/' + $('#MenuItemParentId').val(),
                    success: function(msg){
                        menuItemData = ( msg.length >= 1 ) ? JSON.parse( msg ) : new Array();

                        slug = $('#MenuItemTitle').val();
                        slug = slug.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
                        slug = slug.replace(/-/gi, "_");
                        slug = slug.replace(/\s/gi, "-");
                        prefix = ( menuItemData.MenuItem.link.length >= 1 ) ? menuItemData.MenuItem.link : '';
                        $('#MenuItemLink').val( prefix + '/' + slug.toLowerCase() );
                    }
                });
            }
        });

        // changed by human
        var humanChangedSlug = <?= isset( $this->data['MenuItem']['id'] ) ? 'true' : 'false'; ?>;
        $('#MenuItemLink').change( function() {
            humanChangedSlug = true;
        });
    });

    function updateMenuItems() {

        $.ajax({
            type: "GET",
            url: '/admin/menu_items/ajax_menu_items/' + $('#MenuItemMenuId').val(),
            success: function(msg){
                var menuitems  = ( msg.length >= 1 ) ? JSON.parse( msg ) : new Array();
                var options    = new Array();

                // loop through all menuitems available
                for (var i = 0; i < menuitems.length; i++) {

                    // selected
                    if( menuitems[i].id && menuitems[i].id == $('#MenuItemOldParentId' ).val() ) {
                        options += '<option selected value="' + menuitems[i].id + '">' + menuitems[i].title + '</option>';

                    // non-selected
                    } else {
                        options += '<option value="' + menuitems[i].id + '">' + menuitems[i].title + '</option>';
                    }
                }

                // append to select box
                $('#MenuItemParentId' ).empty().append( options );
            }
        });
    }

    var menuData = new Array;
    getMenu();
    function setMenu( newData ) { menuData = newData; }

    function getMenu() {
        return $.ajax({
            type: "GET",
            url: '/admin/menus/ajax_menu/' + $('#MenuItemMenuId').val(),
            success: function(msg){
                data = ( msg.length >= 1 ) ? JSON.parse( msg ) : new Array();
                setMenu( data );
            }
        });
    }
</script>

<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link', array( "plugin" => "admin", "HistoryModel" => "MenuItem" ) ); ?>
<?= $this->element( 'auto_complete', array( "plugin" => "LayerCake", "field_id" => "MenuItemLink", "dictionary" => isset( $menu_slugs  ) ? $menu_slugs  : array(), "multiple" => false ) ); ?>