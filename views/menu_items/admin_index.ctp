<h1>Menu Items</h1>

<? if( sizeof( $menuItems ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array( 'action' => 'index', 'menu_id' => $this->passedArgs['menu_id'] ) ); ?></p>
<? endif; ?>

<?= $html->link( "New Menu Item", '/admin/menu_items/add/menu_id:' . $this->passedArgs['menu_id'], array( 'class' => 'button', 'escape' => false )  ); ?>
&nbsp;&nbsp;&nbsp;
<?= $html->link( "Back to Menus", '/admin/menus/', array( 'class' => 'button', 'escape' => false )  ); ?>

<? if( sizeof( $menuItems ) >= 1 ): ?>

    <table class="list">
        <tr>
            <th>Title</th>
            <th>Link</th>
            <th>Status</th>
            <th>&nbsp;</th>
            <th>Modified</th>
            <th class="t-center">Actions</th>
        </tr>

        <tbody>
            <? for( $i=0; $i < count( $menuItems ); $i++ ): ?>
                <? $menuItem = $menuItems[$i]; ?>
                <? if( count( $menuItem['children'] ) >= 1 ) {
                    // insert children into array
                    foreach( $menuItem['children'] AS &$child ) {
                        $child['depth'] = isset( $menuItem['depth'] ) ? $menuItem['depth'] + 1 : 1;
                    }

                    array_splice( $menuItems, $i+1, 0, $menuItem['children'] );
                } ?>

                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td>
                        <div <? if( isset( $menuItem['depth'] ) ): ?>style="padding-left: <?= $menuItem['depth'] * 15; ?>px;"<? endif; ?>>
                            <a name="<?= $menuItem['MenuItem']['id']; ?>">↳ </a><?= $menuItem['MenuItem']['title']; ?>
                        </div>
                    </td>
                    <td><?= $menuItem['MenuItem']['link'];      ?></td>
                    <td class="t-center">
                        <? if( $menuItem['MenuItem']['status'] == 'active' ): ?>
                            <?= $html->link( 'Active',   '/admin/menu_items/status/' . $menuItem['MenuItem']['id'] . '/status:inactive/menu_id:' . $this->passedArgs['menu_id'], array( 'escape' => false, 'class' => 'record-scroll active',   'title' => 'Click to make inactive' ) ); ?>
                        <? else: ?>
                            <?= $html->link( 'Inactive', '/admin/menu_items/status/' . $menuItem['MenuItem']['id'] . '/status:active/menu_id:'   . $this->passedArgs['menu_id'], array( 'escape' => false, 'class' => 'record-scroll inactive', 'title' => 'Click to make active' ) ); ?>
                        <? endif; ?>
                    </td>
                    <td class="t-center">
                        <?
                            echo $html->link( '&#8679;', '/admin/menu_items/moveup/'   . $menuItem['MenuItem']['id'] . '/menu_id:' . $this->passedArgs['menu_id'], array( 'escape' => false, 'class' => 'record-scroll', 'style' => 'text-decoration:none; font-weight:bold;', 'title' => "Move Up" ) );
                            echo "&nbsp;";
                            echo $html->link( '&#8681;', '/admin/menu_items/movedown/' . $menuItem['MenuItem']['id'] . '/menu_id:' . $this->passedArgs['menu_id'], array( 'escape' => false, 'class' => 'record-scroll', 'style' => 'text-decoration:none; font-weight:bold;', 'title' => "Move Down" ) );
                        ?>
                    </td>
                    <td><?= date( 'Y-m-d', strtotime( $menuItem['MenuItem']['modified'] )); ?></td>
                    <td class="t-center icons" style="white-space: nowrap;">
                        <?
                            echo $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"   />', '/admin/menu_items/edit/'   . $menuItem['MenuItem']['id'] . '/menu_id:' . $this->passedArgs['menu_id'], array( 'escape' => false, 'class' => 'record-scroll' ) );
                            echo "&nbsp;|&nbsp;";
                            echo $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', '/admin/menu_items/delete/' . $menuItem['MenuItem']['id'] . '/menu_id:' . $this->passedArgs['menu_id'], array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $menuItem['MenuItem']['title'] ) );
                            echo "&nbsp;|&nbsp;";
                            echo $html->link('<img src="/layer_cake/images/ico-mini-show.png"   alt="Show"   />', '/admin/pages/findedit/menu_id:' . $menuItem['MenuItem']['id'], array( 'escape' => false, 'class' => 'record-scroll' ) );
                        ?>
                    </td>
                </tr>
            <? endfor; ?>
        </tbody>
    </table>
<? endif; ?>

<div style="display:none;">
    <div id="dialog-active-status" title="Include Page Content?">
        <p>Select <i>Menu Item + Page Content</i> to update the active/inactive status for the associated page (if any) automatically (recommended).</p>
        <p>Otherwise if you select <i>Menu Item Only</i> you will need to activate or inactive the <b>Page Content</b> manually.</p>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $("a.record-scroll").click( function(ev) {
            ev.preventDefault();

            if( $(this).hasClass( 'active' ) || $(this).hasClass( 'inactive' ) ) {
                var confirmLinkBoth = $(this).attr('href') + "/sy:" + window.scrollY + "/scope:menu+page";
                var confirmLinkOnly = $(this).attr('href') + "/sy:" + window.scrollY + "/scope:menu";

                $( "#dialog-active-status" ).dialog({
                    resizable: false,
                    width: 460,
                    modal: true,
                    buttons: {
                        "Menu Item + Page Content": function() {
                            window.location = confirmLinkBoth;
                        },
                        "Menu Items Only": function() {
                            window.location = confirmLinkOnly;
                        }
                    }
                });
            } else {
                window.location = $(this).attr('href') + "/sy:" + window.scrollY;
            }
        });

        <?php if( $scrollY ): ?>
            window.scrollTo( 0, <?php echo $scrollY; ?> );
        <?php endif; ?>
    });

</script>
