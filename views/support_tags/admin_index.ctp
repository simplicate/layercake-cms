<h1>Support Tags</h1>

<? if( sizeof( $support_tags ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>
<? endif; ?>

<?= $html->link( "New Support Tag", '/admin/support_tags/add', array( 'class' => 'button', 'escape' => false )  ); ?>

<? if( sizeof( $support_tags ) >= 1 ): ?>

    <?
        $locations = array(
            'inside-head'       => 'Inside <head>',
            'after-start-body'  => 'After <body>',
            'before-close-body' => 'Before </body>',
        );
    ?>

    <?= $paginator->options( array( 'url' => $this->passedArgs ) ); ?>
    <table class="list">
        <tr>
            <th><?= $paginator->sort('name');?></th>
            <th><?= $paginator->sort('location');?></th>
            <th class="t-center"><?= $paginator->sort('status');?></th>
            <th class="t-center">Actions</th>
        </tr>

        <tbody>
            <? foreach( $support_tags as $support_tag ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td><?= $support_tag['SupportTag']['name']; ?></td>
                    <td><?= htmlentities( $locations[$support_tag['SupportTag']['location']] ); ?></td>
                    <td class="t-center">
                        <? if( $support_tag['SupportTag']['status'] == 'active' ): ?>
                            <?= $html->link( 'Active',   '/admin/support_tags/status/' . $support_tag['SupportTag']['id'] . '/status:inactive', array( 'escape' => false, 'class' => 'record-scroll active',   'title' => 'Click to make inactive' ) ); ?>
                        <? else: ?>
                            <?= $html->link( 'Inactive', '/admin/support_tags/status/' . $support_tag['SupportTag']['id'] . '/status:active',   array( 'escape' => false, 'class' => 'record-scroll inactive', 'title' => 'Click to make active' ) ); ?>
                        <? endif; ?>
                    </td>

                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"   />', array( 'action' => 'edit',   $support_tag['SupportTag']['id']), array( 'escape' => false ) ); ?>
                        |
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $support_tag['SupportTag']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $support_tag['SupportTag']['name'] ) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>

    <? if( $this->params['paging']['SupportTag']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers(); ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? endif; ?>