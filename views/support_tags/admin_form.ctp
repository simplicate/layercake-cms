<h1>Edit Support Tag</h1>

<?= $form->create( 'SupportTag' );?>
<?= $form->input( 'id' );   ?>
<div class="form-left">
    <?= $form->input( 'name', array( 'label' => 'Reference Name', 'after' => "<div class='small'>Does not appear on site. Only for your reference purposes.<br>i.e. <em>'Conversion Tracking Code'</em> or <em>Google Webmaster Validation</em></div>" ) ); ?>
</div>

<div class="form-right">
    <div class="form-left">
        <?
            $locations = array(
                'inside-head'       => 'Inside <head>',
                'after-start-body'  => 'After <body>',
                'before-close-body' => 'Before </body>',
            );
        ?>

        <?= $form->input( 'location',   array( 'style' => 'width:100%;', 'type' => 'select', 'options' => $locations, 'empty' => false ) ); ?>
    </div>

    <div class="form-right">
        <?= $form->input( 'status', array( 'style' => 'width:100%;', 'type' => 'select', 'options' => array( 'active' => 'Active', 'inactive' => 'Inactive' ), 'label' => 'Active?' ) ); ?>
    </div>
</div>
<div class="clear"></div>

<hr size="1" />
<br />

<?= $form->input( 'content',  array( 'type' => 'textarea' ) ); ?>
<br>

<div class="form-left">
    <? $urls = ( isset( $this->data['SupportTagUrl'] ) && is_array( $this->data['SupportTagUrl'] ) && count( $this->data['SupportTagUrl'] ) >= 1 ) ? $this->data['SupportTagUrl'] : array(); ?>
    <div class="input">
        <label for="SupportTagsUrlsInclude">Include on URLs (One per line)</label>
        <textarea rows="5" id="SupportTagsUrlsInclude" name="data[SupportTag][UrlsInclude]"><? foreach( $urls AS $url ){ if( $url['type'] == 'include' ) { echo $url['url'] . "\n"; } } ?></textarea>
        <div class="small">
            Which URLs <b>SHOULD</b> this tag be included on. Use <em>*</em> for wildcards.<br>
            i.e.<br>
            /products/*<br>
            /about/*<br>
            /contact
        </div>
    </div>
</div>

<div class="form-right">
    <div class="input">
        <label for="SupportTagsUrlsExclude">Exclude from URLs (One per line)</label>
        <textarea rows="5" id="SupportTagsUrlsExclude" name="data[SupportTag][UrlsExclude]"><? foreach( $urls AS $url ){ if( $url['type'] == 'exclude' ) { echo $url['url'] . "\n"; } } ?></textarea>
        <div class="small">
            Which URLs should this tag <b>NOT</b> be included on. i.e.<br>
            i.e.<br>
            /products/widgetA<br>
            /about/history
        </div>
    </div>
</div>
<div class="clear"></div>

<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link', array( "plugin" => "admin", "HistoryModel" => "SupportTag" ) );  ?>