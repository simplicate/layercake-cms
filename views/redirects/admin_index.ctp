<h1>Redirects</h1>

<? if( sizeof( $redirects ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array( 'action' => 'index' ) ); ?></p>
<? endif; ?>

<?= $html->link( "New Redirect", '/admin/redirects/add', array( 'class' => 'button', 'escape' => false )  ); ?>

<? if( sizeof( $redirects ) >= 1 ): ?>

    <?= $paginator->options( array( 'url' => $this->passedArgs ) ); ?>

    <table class="list">
        <tr>
            <th><?= $paginator->sort('URL', 'slug');?></th>
            <th><?= $paginator->sort('Redirect URL', 'redirect_url');?></th>
            <th><?= $paginator->sort('Status', 'status' );?></th>
            <th><?= $paginator->sort('modified');?></th>
            <th class="t-center">Actions</th>
        </tr>

        <tbody>
            <? foreach( $redirects as $redirect ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td><?= $redirect['Redirect']['slug']; ?></td>
                    <td><?= $redirect['Redirect']['redirect_url']; ?></td>
                    <td class="t-center">
                        <? if( $redirect['Redirect']['status'] == 'active' ): ?>
                            <?= $html->link( 'Active',   '/admin/redirects/status/' . $redirect['Redirect']['id'] . '/status:inactive', array( 'escape' => false, 'class' => 'record-scroll active',   'title' => 'Click to make inactive' ) ); ?>
                        <? else: ?>
                            <?= $html->link( 'Inactive', '/admin/redirects/status/' . $redirect['Redirect']['id'] . '/status:active',   array( 'escape' => false, 'class' => 'record-scroll inactive', 'title' => 'Click to make active' ) ); ?>
                        <? endif; ?>
                    </td>

                    <td><?= date( 'Y-m-d', strtotime( $redirect['Redirect']['modified'] )); ?></td>
                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"   />', array( 'action' => 'edit',   $redirect['Redirect']['id']), array( 'escape' => false ) ); ?>
                        |
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $redirect['Redirect']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $redirect['Redirect']['slug'] ) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>


    <? if( $this->params['paging']['Redirect']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers();		 ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? endif; ?>