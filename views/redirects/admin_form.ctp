<h1>Redirect Settings</h1>

<?= $form->create( 'Redirect' ); ?>
<?= $form->input( 'id' );    ?>
<?= $form->input( 'slug', array( 'label' => 'Local URL', 'after' => "<span class='small'>This is the local URL that you want to redirect somewhere else.</span>" ) );  ?>
<?= $form->input( 'redirect_url', array( 'after' => "<span class='small'>This is where you want it to go.</span>" ) );  ?>

<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link', array( "plugin" => "admin", "HistoryModel" => "Redirect" ) ); ?>