<?php
    $layout_options  = array (
        'model'      => 'MenuItem',
        'alias'      => 'title',
        'href_alias' => 'link',
        'type'       => 'ul',
        'itemType'   => 'li',
    );

    $find_conditions = ( isset( $conditions ) && is_array( $conditions ) ) ? $conditions : array();
    $find_conditions['status'] = 'active';

    $user_status = isset( $_SESSION['User']['User']['id'] ) ? 'logged-in' : 'logged-out';
    if( $user_status == 'logged-out' ) {
        $find_conditions['user_status'] = array( 'any', 'out' );
    } else {
        $user_statuses = array( 'any', 'in' );

        if( isset( $_SESSION['User']['UserGroup']['id'] ) ) {
            $user_statuses[] =$_SESSION['User']['UserGroup']['id'];
        }

        $find_conditions['user_status'] = $user_statuses;
    }

    if( isset( $element_id          ) ) { $layout_options['id']         = $element_id;       }
    if( isset( $element_class       ) ) { $layout_options['class']      = $element_class;    }
    if( isset( $element_tmpl        ) ) { $layout_options['element']    = $element_tmpl;     }
    if( isset( $element_type        ) ) { $layout_options['type']       = $element_type;     }
    if( isset( $element_itemType    ) ) { $layout_options['itemType']   = $element_itemType; }
    if( isset( $element_depth       ) ) { $layout_options['depth']      = $element_depth;    }

    $this_url = '/' . $this->params['url']['url'];
    $this_url = preg_replace( '/\/\//', '/', $this_url );
    $this_url = preg_replace( '/\/$/',  '', $this_url );

    App::Import( 'Model',  'Content.MenuItem' );
    $MenuItem = new MenuItem();

    if( isset(  $find_conditions['slug'] ) ) {
        $find_conditions["Menu.slug"]     = $find_conditions['slug'];
        unset( $find_conditions['slug'] );
    }

    $find_conditions["MenuItem.link"] = array( $this_url, $this_url . '/' );

    $menu      = $MenuItem->find( 'first', array( 'conditions' => $find_conditions ) );

    // make sure we're only pulling active menu items
    $MenuItem->Behaviors->detach('Tree');
    $MenuItem->Behaviors->attach('Tree', array(
        'scope' => array(
            "MenuItem.status = 'active'",
            "MenuItem.user_status" => $find_conditions['user_status'],
            "MenuItem.menu_id" => $menu['MenuItem']['menu_id'],
        )
    ));

    $parents = $MenuItem->getpath( $menu['MenuItem']['id'] );

    // if we got passed a layout, handle this real simple
    if( isset($layout) ) {

        echo $this->element( $layout, array( "menu" => $parents, "options" => isset( $options ) ? $options : array() ) );

    // otherwise, get complicated
    } else {
        $MenuTree = new MenuTreeHelper();
        $this->MenuTree = $MenuTree;
        $navigation = $this->MenuTree->generate( $parents, $layout_options );
        echo $navigation;
    }
?>