<?php

    // router url
    $router_url = preg_replace( "/\/$/", "", $this->here );
    //$router_url = preg_replace( "/\/$/", "", Router::url() );
    $router_url = empty( $router_url ) ? "/" : $router_url;

    // what are our find conditions
    $find_conditions = array(
        'Snippet.active'    => 1,
        'Snippet.location'  => isset( $location ) ? $location : '',
        'SnippetUrl.url'    => ( $router_url ) == '/' ? $router_url : $router_url . '/',
        'SnippetUrl.type'   => 'include',
    );

    // are we looking for logged in or logged out snippets?
    if( isset( $_SESSION['User']['User']['id'] ) ) {
        $user_statuses = array( 'any', 'in' );

        if( isset( $_SESSION['User']['UserGroup']['id'] ) ) {
            $user_statuses[] =$_SESSION['User']['UserGroup']['id'];
        }

        $find_conditions['Snippet.user_status'] = $user_statuses;
    } else {
        $find_conditions['Snippet.user_status'] = array( 'any', 'out' );
    }

    // are we looking for a snippet of a particular language
    if( isset( $language ) ) {
        $find_conditions['Snippet.language'] = $language;
    }

    // limit
    $limit = isset( $limit ) ? $limit : 100;

    // import the snippet model
    App::Import( 'Model', 'Content.SnippetUrl' );
    $SnippetUrl  = new SnippetUrl();

    // try to find an exact match
    $snippets = $SnippetUrl->find( 'all', array( 'recursive' => 2, 'conditions' => $find_conditions, 'order' => 'ordering ASC', 'limit' => $limit ) );

    // if we can't find an exact match, try a wild card match
    if( !count( $snippets ) ) {

        // replace all slashes with * so the wildcard match works properly
        $url = str_replace( '/', '*', $find_conditions['SnippetUrl.url'] );

        // build a new find conditions loop
        $wc_find_conditions = array(
            'Snippet.location'      => $find_conditions['Snippet.location'],
            'Snippet.active'        => $find_conditions['Snippet.active'],
            'Snippet.user_status'   => $find_conditions['Snippet.user_status'],
            'SnippetUrl.type'       => 'include',
            "'$url' LIKE CONCAT( url, '%' )",
        );

        // find new snippets
        $snippets = $SnippetUrl->find( 'all', array( 'recursive' => 2, 'conditions' => array( $wc_find_conditions  ), 'order' => array( 'LENGTH(url) DESC', 'ordering ASC' ), 'limit' => $limit ) );
    }

    // does the snippet need a particular class
    $element_class = ( isset( $class ) ) ? $class : 'snippet';

    // loop through all the snippets we found
    foreach( $snippets AS $snippet ) {

        // find urls that this snippet is excluded from
        $this_url       = ( $this->params['url']['url'] ) == '/' ? $this->params['url']['url'] : '/' . $this->params['url']['url'] . '/';
        $this_url       = str_replace( '//', '/', $this_url );
        $skip_snippet   = false;

        // loop through all snippet urls and find the exclude ones
        foreach ( $snippet['Snippet']['SnippetUrl'] as $url ) {
            if( $url['type'] == 'exclude' ) {

                // is there a wildcard match in the exclude?
                if( strstr( $url['url'], '*' ) ) {

                    $needle = rtrim( $url['url'], '*');
                    $length = strlen( $needle );

                    if( substr( $this_url, 0, $length ) === $needle ) {
                        $skip_snippet = true;
                        break;
                    }

                // otherwise this is a direct match
                } else {
                    if( $url['url'] == $this_url ) {
                        $skip_snippet = true;
                        break;
                    }
                }
            }
        }

        // skip this snippet if it's supposed to be explicitly excluded from this page
        if( $skip_snippet == true ) { continue; }

        // if this snippet is based off a template, put that template html into the snippet
        if( isset( $snippet['Snippet']['SnippetTemplate']['id'] ) ) {
            $snippet['Snippet']['content'] = $snippet['Snippet']['SnippetTemplate']['html'];
        }

        // find any <element> tags within the content and extrapolate them out
        if( strstr( $snippet['Snippet']['content'], "<element" ) ) {
            App::Import( 'Helper', 'Content.SnippetElement' );
            $SnippetElement = new SnippetElementHelper();
            $snippet['Snippet']['content'] = $SnippetElement->parse_element_tags( $snippet['Snippet']['content'] );
        }

        // if there's a tag wrapper
        if( isset( $wrapper ) ) {
            echo $this->Html->tag( $wrapper, $snippet['Snippet']['content'], array( 'class' => $element_class ) );

        // otherwise just output the straight html
        } elseif( isset( $element ) ) {
            echo $this->element( $element, array( 'snippet' => $snippet ) );
        } else {
            echo $snippet['Snippet']['content'];
        }
    }
?>