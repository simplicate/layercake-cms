<?php
    $layout_options  = array (
        'model'      => 'MenuItem',
        'alias'      => 'title',
        'href_alias' => 'link',
        'type'       => 'ul',
        'itemType'   => 'li',
    );

    $find_conditions = ( isset( $conditions ) && is_array( $conditions ) ) ? $conditions : array();

    $user_status = isset( $_SESSION['User']['User']['id'] ) ? 'logged-in' : 'logged-out';
    if( $user_status == 'logged-out' ) {
        $find_conditions['user_status'] = array( 'any', 'out' );
    } else {
        $user_statuses = array( 'any', 'in' );

        if( isset( $_SESSION['User']['UserGroup']['id'] ) ) {
            $user_statuses[] =$_SESSION['User']['UserGroup']['id'];
        }

        $find_conditions['user_status'] = $user_statuses;
    }

    if( isset( $element_id          ) ) { $layout_options['id']         = $element_id;       }
    if( isset( $element_class       ) ) { $layout_options['class']      = $element_class;    }
    if( isset( $element_tmpl        ) ) { $layout_options['element']    = $element_tmpl;     }
    if( isset( $element_type        ) ) { $layout_options['type']       = $element_type;     }
    if( isset( $element_itemType    ) ) { $layout_options['itemType']   = $element_itemType; }
    if( isset( $element_depth       ) ) { $layout_options['depth']      = $element_depth;    }

    // find the cached version of this menu if it exists
    //$cache_key  = md5( serialize( array( $find_conditions ) ) );
    //Cache::clear();
    //$menu_cache = Cache::read( 'menus_' . $find_conditions['slug'], 'default' );
    //$menu_cache = ( is_array( $menu_cache ) ) ? $menu_cache : array();
    //$menu       = ( isset( $menu_cache[$cache_key] ) ) ? $menu_cache[$cache_key] : array();

    //if( !count( $menu ) ) {
        $this_url = '/' . $this->params['url']['url'];
        $this_url = preg_replace( '/\/\//', '/', $this_url );
        $this_url = preg_replace( '/\/$/',  '', $this_url );

        App::Import( 'Model',  'Content.MenuItem' );
        $MenuItem = new MenuItem();

        $find_conditions["Menu.slug"]       = $find_conditions['slug'];
        $find_conditions["MenuItem.link"]   = array( $this_url, $this_url . '/' );
        //$find_conditions["MenuItem.hide_in_menu !="] = 1;
        $find_conditions['MenuItem.status'] = 'active';
        unset( $find_conditions['slug'] );

        // find the first menu item
        $menu      = $MenuItem->find( 'first', array( 'conditions' => $find_conditions ) );

        // make sure we're only pulling active menu items
        $MenuItem->Behaviors->detach('Tree');
        $MenuItem->Behaviors->attach('Tree', array(
            'scope' => array(
                "MenuItem.status = 'active'",
                "MenuItem.user_status" => $find_conditions['user_status'],
                "MenuItem.hide_in_menu !=" => 1,
                "MenuItem.menu_id" => $menu['MenuItem']['menu_id'],
            )
        ));


        $parents   = $MenuItem->getpath( $menu['MenuItem']['id'] );
        $menu      = $MenuItem->find( 'first', array(
                        'conditions' => array(
                            'MenuItem.status'           => 'active',
                            'MenuItem.id'               => $parents[0]['MenuItem']['id'],
                            "MenuItem.user_status"      => $find_conditions['user_status'],
                            "MenuItem.hide_in_menu !="  => 1,
                        )
                     ));

        $children = array();
        if( $parents[0]['MenuItem']['id'] ) {
            $children = $MenuItem->children( $parents[0]['MenuItem']['id'] );
        }

        array_unshift( $children, $menu );

        //$menu_cache[$cache_key] = $menu;
        //Cache::write( 'submenus_' . $parents[0]['MenuItem']['id'], $menu_cache, 'default' );
    //}

    if( count( $children ) > 1 ) {
        App::import( 'Helper', 'LayerCake.MenuTree' );
        $layout_options['activeSlug'] = $this_url;
        $MenuTree = new MenuTreeHelper();
        $this->MenuTree = $MenuTree;
        $navigation = $this->MenuTree->generate( $children, $layout_options );
        echo $navigation;
    }
?>