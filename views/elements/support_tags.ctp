<?php

    // router url
    $router_url = preg_replace( "/\/$/", "", $this->here );
    $router_url = empty( $router_url ) ? "/" : $router_url;

    // what are our find conditions
    $find_conditions = array(
        'SupportTag.status'    => 'active',
        'SupportTag.location'  => isset( $location ) ? $location : '',
        'SupportTagUrl.url'    => ( $router_url ) == '/' ? $router_url : $router_url . '/',
        'SupportTagUrl.type'   => 'include',
    );

    // import the support tags model
    App::Import( 'Model', 'Content.SupportTagUrl' );
    $SupportTagUrl  = new SupportTagUrl();

    // try to find an exact match
    $support_tags = $SupportTagUrl->find( 'all', array( 'recursive' => 2, 'conditions' => $find_conditions ) );

    // if we can't find an exact match, try a wild card match
    if( !count( $support_tags ) ) {

        // replace all slashes with * so the wildcard match works properly
        $url = str_replace( '/', '*', $find_conditions['SupportTagUrl.url'] );

        // build a new find conditions loop
        $wc_find_conditions = array(
            'SupportTag.location'      => $find_conditions['SupportTag.location'],
            'SupportTag.status'        => $find_conditions['SupportTag.status'],
            'SupportTagUrl.type'       => 'include',
            "'$url' LIKE CONCAT( url, '%' )",
        );

        // find new support tags
        $support_tags = $SupportTagUrl->find( 'all', array( 'recursive' => 2, 'conditions' => array( $wc_find_conditions  ), 'order' => array( 'LENGTH(url) DESC' ) ) );
    }

    // loop through all the support tags we found
    foreach( $support_tags AS $support_tag ) {

        // find urls that this support tag is excluded from
        $this_url = ( $this->params['url']['url'] ) == '/' ? $this->params['url']['url'] : '/' . $this->params['url']['url'] . '/';
        $this_url = str_replace( '//', '/', $this_url );
        $skip     = false;

        // loop through all support tag urls and find the exclude ones
        foreach ( $support_tag['SupportTag']['SupportTagUrl'] as $url ) {
            if( $url['type'] == 'exclude' ) {

                // is there a wildcard match in the exclude?
                if( strstr( $url['url'], '*' ) ) {

                    $needle = rtrim( $url['url'], '*');
                    $length = strlen( $needle );

                    if( substr( $this_url, 0, $length ) === $needle ) {
                        $skip = true;
                        break;
                    }

                // otherwise this is a direct match
                } else {
                    if( $url['url'] == $this_url ) {
                        $skip = true;
                        break;
                    }
                }
            }
        }

        // skip this tag if it's supposed to be explicitly excluded from this page
        if( $skip == true ) { continue; }

        // output the tag
        echo $support_tag['SupportTag']['content'] . "\n";
    }
?>