<?php
    $layout_options  = array (
        'model'      => 'MenuItem',
        'alias'      => 'title',
        'href_alias' => 'link',
        'type'       => 'ul',
        'itemType'   => 'li',
    );

    $find_conditions = ( isset( $conditions ) && is_array( $conditions ) ) ? $conditions : array();
    $find_conditions['status'] = 'active';
    $find_conditions['MenuItem.hide_in_menu !='] = 1;

    $user_status = isset( $_SESSION['User']['User']['id'] ) ? 'logged-in' : 'logged-out';
    if( $user_status == 'logged-out' ) {
        $find_conditions['user_status'] = array( 'any', 'out' );
    } else {

        $user_statuses = array( 'any', 'in' );

        if( isset( $_SESSION['User']['UserGroup']['id'] ) ) {
            $user_statuses[] =$_SESSION['User']['UserGroup']['id'];
        }

        $find_conditions['user_status'] = $user_statuses;
    }

    if( isset( $element_id          ) ) { $layout_options['id']         = $element_id;       }
    if( isset( $element_class       ) ) { $layout_options['class']      = $element_class;    }
    if( isset( $element_tmpl        ) ) { $layout_options['element']    = $element_tmpl;     }
    if( isset( $element_type        ) ) { $layout_options['type']       = $element_type;     }
    if( isset( $element_itemType    ) ) { $layout_options['itemType']   = $element_itemType; }

    // find the cached version of this menu if it exists
    $cache_key  = md5( serialize( array( $find_conditions ) ) );
    Cache::clear();
    $cache_loc  = isset( $find_conditions['slug'] ) ? $find_conditions['slug'] : 'any';
    $menu_cache = Cache::read( 'menus_' . $cache_loc, 'default' );
    $menu_cache = ( is_array( $menu_cache ) ) ? $menu_cache : array();
    $menu       = ( isset( $menu_cache[$cache_key] ) ) ? $menu_cache[$cache_key] : array();

    if( !count( $menu ) ) {
        App::Import( 'Model',  'Content.MenuItem' );
        $MenuItem = new MenuItem();

        // is one of our conditions a parent link (i.e. this is a sub-list menu)
        $parent_link = '';
        if( !empty( $find_conditions['parent_link'] ) ) {
            $parent_link = $find_conditions['parent_link'];
            unset( $find_conditions['parent_link'] );
        }

        // load the parent menu
        $first_menu = $MenuItem->find( 'first', array( 'conditions' => $find_conditions ) );

        // initial scope
        $scope = array(
            "MenuItem.status"          => 'active',
            "MenuItem.user_status"     => $find_conditions['user_status'],
            "MenuItem.hide_in_menu !=" => 1,
            "MenuItem.menu_id"         => $first_menu['MenuItem']['menu_id'],
        );

        // if we're loading a sub-list
        if( !empty( $parent_link ) ) {
            $sub_menuitem = $MenuItem->find( 'first', array( 'conditions' => array( 'MenuItem.link' => $parent_link, "MenuItem.menu_id" => $first_menu['MenuItem']['menu_id'] ) ) );
            $find_conditions["lft >"]  = $sub_menuitem['MenuItem']['lft'];
            $find_conditions["rght <"] = $sub_menuitem['MenuItem']['rght'];
        }

        // make sure we're only pulling active menu items
        $MenuItem->Behaviors->detach('Tree');
        $MenuItem->Behaviors->attach('Tree', array(
            'scope' => $scope
        ));

        $menu = $MenuItem->find( 'threaded', array( 'conditions' => $find_conditions, 'order' => 'lft ASC' ) );

        $menu_cache[$cache_key] = $menu;
        Cache::write( 'menus_' . $cache_loc, $menu_cache, 'default' );
    }

    if( count( $menu ) >= 1 ) {
        App::import( 'Helper', 'LayerCake.MenuTree' );
        $MenuTree = new MenuTreeHelper();
        $this->MenuTree = $MenuTree;
        $navigation = $this->MenuTree->generate( $menu, $layout_options );
        echo $navigation;
    }
?>