<h1>Snippet Content</h1>

<?= $form->create( 'Snippet' );?>
<?= $form->input( 'id' );   ?>
<div class="form-left">
    <?= $form->input( 'name', array( 'after' => "<span class='note'>Name is just for your reference; it does not appear in the Snippet.</span>" ) ); ?>
</div>

<div class="form-right">

    <div class="form-left">
        <?
            $locations = array();
            foreach( $snippet_locations AS $location ) { $locations[$location] = $location; }
        ?>

        <?= $form->input( 'location',   array( 'type' => 'select', 'options' => $locations, 'empty' => true ) ); ?>
    </div>

    <div class="form-right">
        <?= $form->input( 'ordering', array( 'label' => 'Order of Appearance in Location', 'default' => 1 ) ); ?>
    </div>
    <div class="clear"></div>

    <div class="form-w33">
        <?= $form->input( 'active',                 array( 'type' => 'select', 'options' => array( 1 => 'Active', 0 => 'Inactive' ), 'label' => 'Active?' ) ); ?>
    </div>
    <div class="form-w33">
        <?
            $user_status = array( 'any' => 'All Users', 'out' => 'Logged Out Users', 'in' => 'Logged In Users', '' => "----------" );
            foreach( $user_groups AS $key => $name ) { $user_status[$key] = $name; }
        ?>
        <?= $form->input( 'user_status',            array( 'type' => 'select', 'options' => $user_status, 'label' => 'User Status' ) ); ?>
    </div>
    <div class="form-w33">
        <?= $form->input( 'language',               array( 'type' => 'select', 'options' => array( 'eng' => 'English', 'fre' => 'French' ) ) ); ?>
    </div>
    <div class="clear"></div>

    <? if( count( $snippet_templates ) ): ?>
        <?= $form->input( 'snippet_template_id',    array( 'type' => 'select', 'options' => $snippet_templates, 'empty' => true ) ); ?>
    <? else: ?>
        <?= $form->input( 'snippet_template_id',    array( 'type' => 'hidden' ) ); ?>
    <? endif; ?>
</div>
<div class="clear"></div>

<hr size="1" />
<br />

<div class="form-left">
    <? $urls = ( isset( $this->data['SnippetUrl'] ) && is_array( $this->data['SnippetUrl'] ) && count( $this->data['SnippetUrl'] ) >= 1 ) ? $this->data['SnippetUrl'] : array(); ?>
    <div class="input">
        <label for="SnippetsUrlsInclude">URLs (Include)</label>
        <textarea rows="5" id="SnippetsUrlsInclude" name="data[Snippet][UrlsInclude]"><? foreach( $urls AS $url ){ if( $url['type'] == 'include' ) { echo $url['url'] . "\n"; } } ?></textarea>
    </div>
</div>

<div class="form-right">
    <div class="input">
        <label for="SnippetsUrlsExclude">URLs (Exclude)</label>
        <textarea rows="5" id="SnippetsUrlsExclude" name="data[Snippet][UrlsExclude]"><? foreach( $urls AS $url ){ if( $url['type'] == 'exclude' ) { echo $url['url'] . "\n"; } } ?></textarea>
    </div>
</div>
<div class="clear"></div>

<?= $form->input( 'content',  array( 'class' => 'ck-full', 'div' => 'input textarea content-area' ) ); ?>

<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link', array( "plugin" => "admin", "HistoryModel" => "Snippet" ) );  ?>
<?= $this->element( 'ck_editor',   array( "plugin" => "admin", "type" => array( "regular" ) ) ); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#SnippetSnippetTemplateId').change( function() {
            check_snippet_template();
        });

        check_snippet_template();
    });

    function check_snippet_template() {
        if( $('#SnippetSnippetTemplateId').val() ) {
            $('.content-area').hide();
        } else {
            $('.content-area').show();
        }
    }
</script>