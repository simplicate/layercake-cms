<h1>Content Snippets</h1>

<? if( sizeof( $snippets ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>
<? endif; ?>

<?= $html->link( "New Snippet", '/admin/snippets/add', array( 'class' => 'button', 'escape' => false )  ); ?>

<? if( sizeof( $snippets ) >= 1 ): ?>

    <?= $paginator->options( array( 'url' => $this->passedArgs ) ); ?>
    <table class="list">
        <tr>
            <th><?= $paginator->sort('name');?></th>
            <th><?= $paginator->sort('location');?></th>
            <th><?= $paginator->sort('Order', 'ordering');?></th>
            <th><?= $paginator->sort('Lang', 'language');?></th>
            <th><?= $paginator->sort('modified');?></th>
            <th class="t-center">Actions</th>
        </tr>
        
        <tbody>
            <? foreach( $snippets as $snippet ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td>
                        <b><?= $snippet['Snippet']['name']; ?></b><br />
                        <?= substr( strip_tags( $snippet['Snippet']['content'] ), 0, 140 ); ?>
                    </td>
                    <td><?= $snippet['Snippet']['location']; ?></td>
                    <td><?= $snippet['Snippet']['ordering']; ?></td>
                    <td><?= $snippet['Snippet']['language']; ?></td>
                    <td><?= date( 'Y-m-d', strtotime( $snippet['Snippet']['modified'] )); ?></td>
                    <td class="t-center icons">                            
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"   />', array( 'action' => 'edit',   $snippet['Snippet']['id']), array( 'escape' => false ) ); ?>
                        |
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $snippet['Snippet']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $snippet['Snippet']['name'] ) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>
        
    <? if( $this->params['paging']['Snippet']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>
    
            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers(); ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? endif; ?>