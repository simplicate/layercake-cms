<h1>Content Pages</h1>

<? if( sizeof( $pages ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>
<? endif; ?>

<?= $html->link( "New Page", '/admin/pages/add', array( 'class' => 'button', 'escape' => false )  ); ?>

<? if( sizeof( $pages ) >= 1 ): ?>

    <?
        $paginator_query = isset( $this->params['named']['q'] ) ? $this->params['named']['q'] : '';
        $this->Paginator->options( array('url' => array( "q:" . $paginator_query ) ) );
    ?>

    <table class="list">
        <tr>
            <th><?= $paginator->sort('name');?></th>
            <th><?= $paginator->sort('URL', 'slug' );?></th>
            <th class="t-center"><?= $paginator->sort('Status', 'status' );?></th>
            <th class="t-center"><?= $paginator->sort('Lang', 'language');?></th>
            <th class="t-center"><?= $paginator->sort('modified');?></th>
            <th class="t-center">Actions</th>
        </tr>

        <tbody>
            <? foreach( $pages as $page ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td>
                        <b><?= $page['Page']['name']; ?></b><br />
                        <?= substr( strip_tags( $page['Page']['content'] ), 0, 140 ); ?>
                    </td>
                    <td><a href="<?= $page['Page']['slug']; ?>" target="_blank"><?= $page['Page']['slug']; ?></a></td>
                    <td class="t-center">
                        <? if( $page['Page']['status'] == 'active' ): ?>
                            <?= $html->link( 'Active',   '/admin/pages/status/' . $page['Page']['id'] . '/status:inactive', array( 'escape' => false, 'class' => 'record-scroll active',   'title' => 'Click to make inactive' ) ); ?>
                        <? else: ?>
                            <?= $html->link( 'Inactive', '/admin/pages/status/' . $page['Page']['id'] . '/status:active',   array( 'escape' => false, 'class' => 'record-scroll inactive', 'title' => 'Click to make active' ) ); ?>
                        <? endif; ?>
                    </td>
                    <td class="t-center"><?= $page['Page']['language']; ?></td>
                    <td class="t-center"><?= date( 'Y-m-d', strtotime( $page['Page']['modified'] )); ?></td>
                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"   />', array( 'action' => 'edit',   $page['Page']['id']), array( 'escape' => false ) ); ?>
                        |
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $page['Page']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $page['Page']['name'] ) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>

    <? if( $this->params['paging']['Page']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers();		 ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? endif; ?>

<div style="display:none;">
    <div id="dialog-active-status" title="Include Menu Items?">
        <p>If this page has <b>Menu Items</b> select <i>Page Content + Menu Items</i> to update their active/inactive status automatically (recommended).</p>
        <p>Otherwise if you select <i>Page Content Only</i> you will need to activate or inactive individual menu items manually.</p>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("a.record-scroll").click( function(ev) {
            ev.preventDefault();

            if( $(this).hasClass( 'active' ) || $(this).hasClass( 'inactive' ) ) {
                var confirmLinkBoth = $(this).attr('href') + "/sy:" + window.scrollY + "/scope:page+menu";
                var confirmLinkOnly = $(this).attr('href') + "/sy:" + window.scrollY + "/scope:page";

                $( "#dialog-active-status" ).dialog({
                    resizable: false,
                    //height:140,
                    width: 460,
                    modal: true,
                    buttons: {
                        "Page Content + Menu Items": function() {
                            window.location = confirmLinkBoth;
                        },
                        "Page Content Only": function() {
                            window.location = confirmLinkOnly;
                        }
                    }
                });
            } else {
                window.location = $(this).attr('href') + "/sy:" + window.scrollY;
            }
        });

        <?php if( $scrollY ): ?>
            window.scrollTo( 0, <?php echo $scrollY; ?> );
        <?php endif; ?>
    });
</script>