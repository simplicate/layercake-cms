<?
    $header = "Search Results";
    $this->set( 'title_for_layout', $header );
    $this->set( 'h1_for_layout',    $header );
?>

<h1><?= $header; ?></h1>

<? foreach ( $results as $result ): ?>
    <p>
        <b><a href="<?= $result['SearchIndex']['link']; ?>"><?= $result['SearchIndex']['name']; ?></a></b><br />
        <?= snippet_text( $result['SearchIndex']['summary'], $search_term ); ?><br />
        <span><a href="<?= $result['SearchIndex']['link']; ?>">http://<?= $_SERVER['HTTP_HOST']; ?><?= $result['SearchIndex']['link']; ?></a></span>
    </p>
<? endforeach; ?>

<?
    function snippet_text( $text, $keyword ) {
        $text    = strip_tags( $text );
        $size    = 90;
        $start   = stripos( $text, $keyword ) - $size;

        if( $start < 0 ) {
            $size += ( abs( $start ) );
            $start = 0;
        }

        $end     = stripos($text, $keyword) + strlen($keyword) + $size;
        $length  = $end - $start;

        $snippet = substr( $text, $start, $length );
        $snippet = str_ireplace( $keyword, '<strong>'.$keyword.'</strong>', $snippet );

        return $snippet;
    }
?>

<? if( $paginator->hasPage(2) ): ?>
    <p class="pagination" style="text-align:center;">
        <?= $paginator->prev('«'); ?>
        <?= $paginator->numbers();		 ?>
        <?= $paginator->next('»'); ?>
    </p>
<? endif; ?>