<?php
    // get the header
    $header = $page['Page']['name'];
    $header = preg_replace( '/^\//', '', $header );
    $header = ucfirst( $header );

    // find any <element> tags within the content and extrapolate them out
    if( strstr( $page['Page']['content'], "<element" ) ) {
        App::Import( 'Helper', 'Content.SnippetElement' );
        $SnippetElement = new SnippetElementHelper();
        $page['Page']['content'] = $SnippetElement->parse_element_tags( $page['Page']['content'] );
    }

    // set the page title
    $this->set( 'title_for_layout', $header );
    $this->set( 'header', $page['Page']['name'] );

    $ogmeta = array(
        'title'         => empty( $page['Page']['title'] ) ? $page['Page']['name'] : $page['Page']['title'],
        'description'   => $page['Page']['meta_description'],
    );

    $this->set( 'ogmeta', $ogmeta );
?>

<?= $page['Page']['content']; ?>