<h1>Page Content</h1>

<?= $form->create( 'Page', array( 'id' => 'PageForm' ) );?>
<?= $form->input( 'id' ); ?>


<div class="form-left">
    <?= $form->input( 'name' ); ?>

    <? if( !isset( $this->data['Page']['id'] ) ): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#PageName').blur( function() {

                    var page_name = $('#PageName').val()
                        .toLowerCase()
                        .replace(/[^\w ]+/g,'')
                        .replace(/ +/g,'-');

                    $('#PageSlug').val( "/" + page_name );
                });
            });
        </script>
    <? endif; ?>

    <?= $form->input( 'slug', array( 'label' => 'URL', 'after' => "<span class='small' style='font-size:11px;'>Try not to update this URL once the page is created unless absolutely necessary</span>" ) ); ?>

    <div class="input">
        <?
            $image_1 = "";
            if( !empty( $this->data['Page']['header_image'] ) ) {
                $image_1 = '<img id="PageHeaderImageSrc" width="461" src="' . $this->data['Page']['header_image'] . '" class="thumbnail" />';
            } else {
                $image_1 = '<img id="PageHeaderImageSrc" width="461" src="" class="thumbnail hidden" />';
            }
        ?>
        <?= $form->input( 'header_image', array( 'label' => 'Header Image', 'between' => $image_1, 'after' => '&nbsp;<input type="button" value="Browse Server" onclick="BrowseServer( \'Images:/page-headers\', \'PageHeaderImage\' );" />' ) ); ?>
    </div>

    <script type="text/javascript">
        $('#PageHeaderImage').change( function() {
            $('#PageHeaderImageSrc').attr( 'src', $(this).val() ).show();
        });
    </script>
</div>

<div class="form-right">

    <div class="form-w33">
        <?= $form->input( 'layout_id', array( 'type' => 'select', 'options' => $layouts ) ); ?>
    </div>
    <div class="form-w33">
        <?= $form->input( 'language', array( 'type' => 'select', 'options' => array( 'eng' => 'English', 'fre' => 'French' ) ) ); ?>
    </div>
    <div class="form-w33">
        <?
            $user_status = array( 'any' => 'All Users', 'out' => 'Logged Out Users', 'in' => 'Logged In Users', '' => "----------" );
            foreach( $user_groups AS $key => $name ) { $user_status[$key] = $name; }
        ?>

        <?= $form->input( 'user_status', array( 'type' => 'select', 'options' => $user_status, 'label' => 'User Status' ) ); ?>
    </div>
    <div class="clear"></div>

    <?= $form->input( 'title', array( 'label' => 'Browser Title' ) ); ?>
    <?= $form->input( 'meta_description',  array( 'type' => 'textarea', 'rows' => 4 ) ); ?>
</div>
<div class="clear"></div>

<?= $form->input( 'content',  array( 'class' => 'ck-full', 'wrap' => 'off' ) ); ?>

<?= $form->end( array( 'label' => 'Save' ) );?>

<div class="submit">
    <input type="button" id="buttonPreview" value="Preview">
</div>

<?= $this->element( 'cancel_link', array( "plugin" => "admin", "HistoryModel" => "Page" ) ); ?>

<script type="text/javascript">

    $(document).ready(function() {
        $('#buttonPreview').click( function(ev) {

            ev.preventDefault();

            for ( instance in CKEDITOR.instances ) {
                CKEDITOR.instances[instance].updateElement();
            }

            dataString = $("#PageForm").serialize();

            $.ajax({
                type: "POST",
                url: '/admin/pages/preview/' + $('#PageId').val(),
                data: dataString,
                success: function( msg ) {
                    var returnMsg = JSON.parse( msg );
                    var url = 'http://<?= $_SERVER['HTTP_HOST']; ?>' + $('#PageSlug').val() + '?mode=preview&page_history_id=' + returnMsg.id;
                    var windowName = "_preview";
                    window.open(url, windowName);
                }
            });

            return false;
        });
    });
</script>

<?= $this->element( 'ck_editor', array( "plugin" => "admin", "type" => array( "regular" ) ) ); ?>
<?= $this->element( 'auto_complete', array( "plugin" => "LayerCake", "field_id" => "PageSlug", "dictionary" => isset( $menu_slugs  ) ? $menu_slugs  : array(), "multiple" => false ) ); ?>
<?= $this->element( 'file_browser',  array( "plugin" => "LayerCake" ) ); ?>

<br /><br />
<? if( isset( $histories ) && count( $histories ) >= 1 ): ?>
    <div id="history" style="display:none;">
        <h2>Page History</h2>
        <table class="list">
            <tr>
                <th>Date</th>
                <th>Review</th>
                <th>Action</th>
            </tr>

            <tbody>
                <? foreach( $histories as $key => $history ): ?>
                    <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                        <td><?= date( 'Y-m-d H:i:s', strtotime( $history['PageHistory']['created'] )); ?></td>
                        <td><a target="_preview" href="http://<?= $_SERVER['HTTP_HOST']; ?><?= $this->data['Page']['slug']; ?>?mode=preview&page_history_id=<?=$history['PageHistory']['id'];?>">Review</a></td>
                        <td class="t-center icons">
                            <? if( $key == 0 ): ?>
                                Current
                            <? else: ?>
                                <?= $html->link('Load this History', array( 'action' => 'revert', $history['PageHistory']['id']), array(), 'Are you sure you want to revert to this history?' ); ?>
                            <? endif; ?>
                        </td>
                    </tr>
                <? endforeach; ?>
            </tbody>
        </table>
    </div>

    <a href="javascript:void(0);" id="page-history">Page Revision History</a>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#page-history').click(function() {
              $('#history').toggle();
            });
        });
    </script>
<? endif; ?>