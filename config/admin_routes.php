<?
    Router::connect( '/admin/pages',                     array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'pages', 'action' => 'index' ) );
    Router::connect( '/admin/pages/:action/*',           array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'pages' ) );

    Router::connect( '/admin/redirects',                 array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'redirects', 'action' => 'index' ) );
    Router::connect( '/admin/redirects/:action/*',       array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'redirects' ) );

    Router::connect( '/admin/snippets',                  array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'snippets', 'action' => 'index' ) );
    Router::connect( '/admin/snippets/:action/*',        array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'snippets' ) );

    Router::connect( '/admin/support_tags',              array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'support_tags', 'action' => 'index' ) );
    Router::connect( '/admin/support_tags/:action/*',    array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'support_tags' ) );

    Router::connect( '/admin/menus',                     array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'menus', 'action' => 'index' ) );
    Router::connect( '/admin/menus/:action/*',           array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'menus' ) );

    Router::connect( '/admin/menu_items',                array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'menu_items', 'action' => 'index' ) );
    Router::connect( '/admin/menu_items/:action/*',      array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'menu_items' ) );

    Router::connect( '/admin/files',                     array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'content', 'controller' => 'files', 'action' => 'index' ) );