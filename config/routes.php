<?
    // static content routing
    App::import( 'Lib', 'Content.ContentRoute' );
    Router::connect( '/*', array( 'plugin' => 'content', 'controller' => 'pages',     'action' => 'view'    ), array( 'routeClass' => 'ContentRoute' ) );

    // redirect routing
    App::import( 'Lib', 'Content.RedirectRoute' );
    Router::connect( '/*', array( 'plugin' => 'content', 'controller' => 'redirects', 'action' => 'gotourl' ), array( 'routeClass' => 'RedirectRoute' ) );


    Router::connect( '/error404', array( 'plugin' => 'content', 'controller' => 'pages', 'action' => 'error404' ) );
    Router::connect( '/error401', array( 'plugin' => 'content', 'controller' => 'pages', 'action' => 'error401' ) );